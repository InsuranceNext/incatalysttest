import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, ActivatedRoute,NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized  } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams,HttpClientModule } from '@angular/common/http';
import { MatDialogModule} from '@angular/material';
import { IdeapopupService } from '../../services/ideapopup.service';
import { CampaignsService } from '../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';
import { UploadService} from 'src/app/services/upload.service';

import { CampaignsubmitideaComponent } from './campaignsubmitidea.component';

describe('CampaignsubmitideaComponent', () => {
  let component: CampaignsubmitideaComponent;
  let fixture: ComponentFixture<CampaignsubmitideaComponent>;
  let authService: AuthService;
  let router:ActivatedRoute;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:
      [
        RouterTestingModule,
        HttpClientModule,
        BrowserModule,
        FormsModule,
        MatDialogModule
      ],
      declarations: [ CampaignsubmitideaComponent ],
      providers:[AuthService,
      {
        provide: ActivatedRoute,
        useValue: {
          data:{
               subscribe: (fn: (value: data) => void) => fn({
                   campaign: [{user:{}}],
               })
           },
           snapshot: {
            params:
                {

                }
              }
      }
    }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const cognitoUser = {
    getSession: (callback) => {
      return null;
    },
  getUsername:()=> {
    return "User123";
  }
};

  const authService = TestBed.get(AuthService);
  const authUserSpy = spyOn(authService, 'getAuthenticatedUser').and.returnValue(cognitoUser);
  fixture = TestBed.createComponent(CampaignsubmitideaComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('Submit ideas for the Campaign', () => {
	let component: CampaignsubmitideaComponent;
	let upload: UploadService;
	let ideaPoup:IdeapopupService;
	let fixture: ComponentFixture<CampaignsubmitideaComponent>;
	let dialog: MatDialogModule;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CampaignsubmitideaComponent],
			providers: [UploadService, IdeapopupService],
			imports:[MatDialogModule,FormsModule]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CampaignsubmitideaComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should upload the attachment to s3 Bucket', () => {
		spyOn(upload, "uploadideadoc");
		component.upload();
		expect(upload.uploadideadoc).toHaveBeenCalled();
	});

	it('should dowload the idea attachment from S3 Bucket', () => {
		spyOn(upload, "downloadideadoc").and.returnValue({});
		let docurl = "http://s3.amazonaws.com/640343/test.xls";
		component.download(docurl);
		expect(upload.downloadideadoc).toHaveBeenCalled();
	});

	it('should check for the idea submission', () => {
		spyOn(component, "saveIdea");
		component.save();
		expect(component.saveIdea).toHaveBeenCalled();
	});

	it('should upload the image or video to s3 bucket', () =>{
		spyOn(upload,"uploadideadoc");
		component.upload();
		expect(upload.uploadideadoc).toHaveBeenCalled();
		expect(component.idea.idea_url).toBeTruthy();
	});

	it('should check for wizard function call', () =>{
		spyOn(component,"checkwizard");
		component.ngDoCheck();
		expect(component.checkwizard).toHaveBeenCalled();
		expect(component.customervalue).toEqual("unchecked");
	});

	it('should set the current BMC title for the popup', () => {
		spyOn(ideaPoup,"checkservice");
		spyOn(dialog,"open");
		component.openbmccreate("customersandrelationships");
		expect(ideaPoup.checkservice).toHaveBeenCalled();
		expect(dialog.open).toHaveBeenCalled();
	});

	it('should download and play the Campaign Video', ()=>{
		spyOn(upload,"downloadideadoc");
		spyOn(dialog,"open");
		let videourl="http://s3.amazonaws.com/640343/test.mp4";
		let campaignName="Test Campaign"
		component.openvideoplay(videourl,campaignName);
		expect(upload.downloadideadoc).toHaveBeenCalled();
		expect(dialog.open).toHaveBeenCalled();
	})

	it('should check whether answer is defined', ()=>{
		let answer={"Founder":["test"],"ValueProposition":["Thsr"]};
		expect(component.checknullstring(answer)).toBe(true);
	})
});

