import { Component,OnInit} from '@angular/core';
import { Router} from '@angular/router';
import { ActivatedRoute} from '@angular/router';

import { IdeapopupService} from '../../services/ideapopup.service';
import { IdeasService} from '../../services/ideas.service';
import { AuthService} from '../../services/auth/auth.service';
import { OthersService} from 'src/app/services/others.service';
import { UploadService} from 'src/app/services/upload.service';

import { MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material';
import { IdeapopupComponent} from './../ideapopup/ideapopup.component';
import { PlayvideoComponent} from './../playvideo/playvideo.component';


@Component({
    selector: 'app-campaignsubmitidea',
    templateUrl: './campaignsubmitidea.component.html',
    styleUrls: ['./campaignsubmitidea.component.css']
})

export class CampaignsubmitideaComponent implements OnInit {
    _data: any;
    file: any;
    fileContent: any = '';
    fileContent1: any = '';
    preSignedUrl: string = 'https://www.google.com';

    campaign: any;
    other: any;
    currentnav: any = 'campaigndetails';
    idea: any = {};
    selectedFiles: any;
    template_type: any;
    customervalue: any;
    foundervalue: any;
    valuepropositionvalue: any;
    keyactivitiesvalue: any;
    keyresourcesvalue: any;
    coststructurevalue: any;
    revenuemodelvalue: any;
    channelvalue: any;
    partnersvalue: any;
    error: any;
    form: any = {};
    opened: boolean = false;
    openedend: boolean = false;
    nobmcentry: String = "false";
    showquestion: boolean = false;
    token: any;

    videourlpopup:any;
    questionarray: any = [{
        "Customer": [{
            "question": "Which classes are you creating values for?\nWho is your most important customer?"
        }]
    }, {
        "Founder": [{
            "question": "What relationship that the target customer expects you to establish?\nHow can you integrate that into your business in terms of cost and format?"
        }]
    }, {
        "ValueProposition": [{
            "question": " What core value do you deliver to the customer?\nWhich customer needs are you satisfying?"
        }]
    }, {
        "Channel": [{
            "question": "Through which channels that your customers want to be reached?\nWhich channels work best? How much do they cost? How can they be integrated into your and your customers routines?"
        }]
    }, {
        "KeyActivities": [{
            "question": "What key activities does your value proposition require?\nWhat activities are important the most in distribution channels, customer relationships, revenue stream?"
        }]
    }, {
        "Partners": [{
            "question": "Who are your key partners/suppliers?.\nWhat are the motivations for the partnerships?"
        }]
    }, {
        "KeyResources": [{
            "question": "What key resources does your value proposition require?\nWhat resources are important the most in distribution channels, customer relationships, revenue stream?"
        }]
    }, {
        "CostStructure": [{
            "question": "What are the most cost in your business?\nWhich key resources/ activities are most expensive?"
        }]
    }, {
        "RevenueModel": [{
            "question": "For what value are your customers willing to pay?\nWhat and how do they recently pay? How would they prefer to pay?\nHow much does every revenue stream contribute to the overall revenues?"
        }]
    }];


    constructor(private router: ActivatedRoute, private route: Router, private ideaPopupService: IdeapopupService, private ideaService: IdeasService, public dialog: MatDialog, private auth: AuthService, private otherservice: OthersService, private uploadservice: UploadService) {
        this.idea.user = this.auth.getAuthenticatedUser().getUsername();

    }

    ngOnInit() {
        this.auth.getAuthenticatedUser().getSession((err, session) => {
            this.token = session.getIdToken().getJwtToken();
        });
        console.log(this.router);
        this.router.data.subscribe(data => {
            this.campaign = data.campaign[0];

            if(this.campaign.campaignimage)
            {
            this.uploadservice.downloadideadoc(this.campaign.campaignimage, this.token).subscribe((data: any) => {
              this.campaign.campaignimage=data;
            });
          }

          this.template_type=this.campaign.template_type;

                if (this.router.snapshot.params.ideaid) {
                  this.currentnav='ideasubmission';

                    this.ideaService.getideabyid(this.router.snapshot.paramMap.get('ideaid'), this.token).subscribe((data:any[])=> {

                        this.other = JSON.parse(data[0].questions);
                        this.idea = data[0];
                        this.idea.questions = JSON.parse(data[0].questions);
                        this.idea.answers = JSON.parse(data[0].answers);
                        if(this.campaign.template_type!='Other')
                        this.prefill(this.idea.answers);
                        this.idea.user = data[0].user._id;

                    })
                } else{

                  if(this.campaign.template_type=='Other') {
                    this.otherservice.getquestionbyid(this.campaign.other_id, this.token).subscribe((data:any[]) => {
                        this.other = [];
                        this.idea.questions = [];
                        for (var i = 0; i < data[0].otherquestions.length; i++) {
                            this.idea.questions[i] = JSON.parse(data[0].otherquestions[i]);
                            this.other[i] = JSON.parse(data[0].otherquestions[i]);
                        }
                        this.idea.answers = [];
                        this.idea.campaign_id = this.campaign._id;
                    })
                }
                else{
                  this.reset();
                }
              }

        });
    }

    reset(){
      this.ideaPopupService.customer('');
      this.ideaPopupService.founders('');
      this.ideaPopupService.valueproposition('');
      this.ideaPopupService.keyactivities('');
      this.ideaPopupService.keyresources('');
      this.ideaPopupService.coststructure('');
      this.ideaPopupService.revenuemodel('');
      this.ideaPopupService.channel('');
      this.ideaPopupService.partners('');
    }
    prefill(answers){
      this.ideaPopupService.customer(answers.Customer[0]);
      this.ideaPopupService.founders(answers.Founder[0]);
      this.ideaPopupService.valueproposition(answers.ValueProposition[0]);
      this.ideaPopupService.keyactivities(answers.KeyActivities[0]);
      this.ideaPopupService.keyresources(answers.KeyResources[0]);
      this.ideaPopupService.coststructure(answers.CostStructure[0]);
      this.ideaPopupService.revenuemodel(answers.RevenueModel[0]);
      this.ideaPopupService.channel(answers.Channel[0]);
      this.ideaPopupService.partners(answers.Partners[0]);
    }
  ngDoCheck(){
    this.checkwizard();
  }

    checkwizard() {
        this.customervalue = this.ideaPopupService.customervalue;
        this.foundervalue = this.ideaPopupService.foundervalue;
        this.valuepropositionvalue = this.ideaPopupService.valuepropositionvalue;
        this.keyactivitiesvalue = this.ideaPopupService.keyactivitiesvalue;
        this.keyresourcesvalue = this.ideaPopupService.keyresourcesvalue;
        this.coststructurevalue = this.ideaPopupService.coststructurevalue;
        this.revenuemodelvalue = this.ideaPopupService.revenuemodelvalue;
        this.channelvalue = this.ideaPopupService.channelvalue;
        this.partnersvalue = this.ideaPopupService.partnersvalue;

    }

    fileChange(e: any) {

        this.file = e.target.files[0];
        const fileReader: FileReader = new FileReader();
        const fileReader1: FileReader = new FileReader();

        fileReader.onloadend = (e) => {
            this.fileContent = fileReader.result;
        };

        fileReader1.onloadend = (e) => {
            this.fileContent1 = fileReader1.result;
        };

        fileReader.readAsArrayBuffer(this.file);
        fileReader1.readAsDataURL(this.file);
    }

    upload() {

      if(this.file&&(!this.file.type.startsWith("image/")&&!this.file.type.startsWith("video/")))
      {
        var filename = this.file.name;
        const base64File = this.fileContent1.split(',')[1];
        this.uploadservice.uploadideadoc(base64File, filename, this.token).subscribe((data: any) => {
            this.idea.idea_url = data.key;
        })
      }
      else{
        alert("Please Choose a File before Upload");
      }
    }

    download(docurl) {
        this.uploadservice.downloadideadoc(docurl, this.token).subscribe((data: any) => {
            window.open(data);

        })
    }

    openbmccreate(bmcpagename: String) {
        this.nobmcentry = "false";

        if (!this.idea.hasOwnProperty('answers')) {
            this.idea.answers = {
                Founder: [],
                Customer: [],
                ValueProposition: [],
                Channel: [],
                KeyActivities: [],
                Partners: [],
                KeyResources: [],
                CostStructure: [],
                RevenueModel: [],
            }
        }
        else{

        }

      this.ideaPopupService.checkservice(bmcpagename, this.idea, this.questionarray);
        let dialogRef = this.dialog.open(IdeapopupComponent, {
            width: '1366px',
            disableClose: true
        });
        // dialogRef.afterClosed().subscribe(result => {
        // this.idea = result.data;
        // });
    }

    openvideoplay(videourl,campaignname){
      if(videourl.startsWith("https://"))
      {
      var videourl=videourl.slice(videourl.indexOf("campaignvideos"),videourl.length);
      }
      else
      {
        var videourl=videourl;
      }


      this.uploadservice.downloadideadoc(videourl, this.token).subscribe((data: any) => {

      let dialogRef = this.dialog.open(PlayvideoComponent, {
          width: '800px',

          disableClose: true,
          data:{"url":data,"campaignname":campaignname}
      });
      // dialogRef.afterClosed().subscribe(result => {
      //     this.idea = result.data;
      // });

    })


    }

    save() {
        this.idea.campaign_id = this.campaign._id;
        this.idea.template_type = this.campaign.template_type;

        if(this.idea.template_type!="Other")
        delete this.idea.other_id;
        else
        this.idea.other_id = this.campaign.other_id;
        // TODO: move create/update logic to service
        if(this.campaign.template_type!='Other')
        {

          //&& this.idea.answers.Customer.length!=0 && this.idea.answers.Founder.length!=0 && this.idea.answers.ValueProposition.length!=0 && this.idea.answers.Channel.length!=0 && this.idea.answers.KeyActivities.length!=0 && this.idea.answers.Partners.length!=0 && this.idea.answers.KeyResources.length!=0 && this.idea.answers.CostStructure.length!=0 && this.idea.answers.RevenueModel.length!=0
        if ((this.idea.answers && this.idea.name && this.idea.keywords && this.idea.description && this.idea.founder_id && this.idea.founder_name)) {
          var validstate=this.checknullstring(this.idea.answers);
          if(validstate)
          {
            this.nobmcentry = "false";
            this.saveIdea(this.idea);
          }
          else{
            alert("Please answer all the Questions");
          }
        }
        // (!this.idea.name||!this.idea.description||!this.idea.keywords||!this.idea.founder_id||!this.idea.founder_name)
        else
        {
          alert("Please Fill All Idea Form Details");
        }
      }
        else {
            this.nobmcentry = "true";
            if(!this.idea.name||!this.idea.description||!this.idea.keywords||!this.idea.founder_id||!this.idea.founder_name){
              alert("Please Fill All Idea Form Details");
            }
            else{
            let isanswer = false;
            let i = 0;
            let len = this.other.length;
            for (i = 0; i < len; i++) {
                if (this.idea.answers && this.idea.answers[i])
                    continue;
                else
                    break;
            }
            if (i == len)
                isanswer = true;
            if (isanswer) {
                this.saveIdea(this.idea);
            } else {
                alert("Please Answer All the Questions");
            }
          }
        }
    }

    checknullstring(answers){
      for(var key in answers) {
       if(answers.hasOwnProperty(key))
          if(answers[key][0]==null||answers[key][0]==undefined||answers[key][0].length==0)
          return false;
           }
           return true;
    }

    saveIdea(idea: any) {

        if (idea != null && idea != undefined) {
            this.ideaService.saveIdeaForCampaign(idea, this.token).subscribe(data => {
                this.route.navigate(['/ideation/campaigns'], {
                    relativeTo: this.router
                });
                // redirect to campaign list
            }, error => {


            })
        }

    }

}
