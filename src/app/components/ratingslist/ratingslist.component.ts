import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

import { CampaignsService } from './../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';
import { UploadService} from 'src/app/services/upload.service';

@Component({
  selector: 'app-ratingslist',
  templateUrl: './ratingslist.component.html',
  styleUrls: ['./ratingslist.component.css']
})
export class RatingslistComponent implements OnInit {
ratingCampaigns:any=[];
desc:String = '';
token:any;
  constructor(private campaignservice: CampaignsService,private route:Router,private router: ActivatedRoute,private auth:AuthService,private uploadservice:UploadService) {

   }

  ngOnInit() {
    this.auth.getAuthenticatedUser().getSession((err, session) => {
      this.token = session.getIdToken().getJwtToken();
        });
         this.auth.getcurrentuserfromdb().subscribe((data:any)=>{
        this.campaignservice.getcampaignsbyuserrating(data[0].email,this.token).subscribe((data:any[]) => {
          if(data.length==0)
          this.ratingCampaigns=[];
          else
          {

              data.forEach(campaign=>{
                this.download(campaign);

              })

          }


     });
   });
  }
  download(campaign) {

    if(campaign.campaignimage)
    {

      this.uploadservice.downloadideadoc(campaign.campaignimage, this.token).subscribe((data: any) => {
        
        campaign.campaignimage=data;
        this.ratingCampaigns.push(campaign);
        })
    }
    else{

      this.ratingCampaigns.push(campaign);
    }
  }

}
