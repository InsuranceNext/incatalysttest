import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingslistComponent } from './ratingslist.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CampaignsService } from 'src/app/services/campaigns.service';
import { Observable, of } from 'rxjs';
import { UploadService } from 'src/app/services/upload.service';
import { Router, ActivatedRoute } from '@angular/router';

describe('Given RatingslistComponent is initialized', () => {
  let component: RatingslistComponent;
  let fixture: ComponentFixture<RatingslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      declarations: [RatingslistComponent],
      providers: [AuthService, { provide: CampaignsService, useValue: {} }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingslistComponent);
    component = fixture.componentInstance;

    const cognitoUser = {
      getSession: (callback) => {
        return null;
      },
    };

    const authService = TestBed.get(AuthService);
    const authUserSpy1 = spyOn(authService, 'getAuthenticatedUser').and.returnValue(cognitoUser);
    const authUserSpy2 = spyOn(authService, 'getcurrentuserfromdb').and.returnValue(of('some value'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('then there should be property token', () => {
    expect(component.token).toBeTruthy();
  });

  it('then there should be property desc', () => {
    expect(component.desc).toBeTruthy();
  });

  it('then there should be property ratingCampaigns', () => {
    expect(component.ratingCampaigns).toBeTruthy();
  });

});


describe('Given that download() method is called, ', () => {

  let uploadService: UploadService;
  let component: RatingslistComponent;
  let fixture: ComponentFixture<RatingslistComponent>;


  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RatingslistComponent],
      providers: [AuthService, UploadService, Router, ActivatedRoute, CampaignsService]
    });

    fixture = TestBed.createComponent(RatingslistComponent);
    component = fixture.componentInstance;
    uploadService = TestBed.get(UploadService);

    component.download({});

  });

  it('then, downloadideadoc() method of upload service should be called', () => {
    spyOn(uploadService, 'downloadideadoc').and.returnValue({});
    expect(uploadService.downloadideadoc).toHaveBeenCalled();

  });


  it('then, the ratingCampaign array length would increase by one', () => {
    spyOn(uploadService, 'downloadideadoc').and.returnValue({});

    const arrayLength = component.ratingCampaigns.length;

    expect(component.ratingCampaigns.length).toEqual(arrayLength + 1);

  });


});



describe('Given that ratingCampaign array has more than one elements, ', () => {

  let component: RatingslistComponent;
  let fixture: ComponentFixture<RatingslistComponent>;


  beforeEach(() => {
    fixture = TestBed.createComponent(RatingslistComponent);
    component = fixture.componentInstance;
  });

  it('the array length should show up on the DOM.', () => {
    const htmlElement: HTMLElement = fixture.nativeElement;
    const divElement = htmlElement.querySelector('div.campaignCount');
    fixture.detectChanges();

    expect(divElement.textContent).toContain(component.ratingCampaigns.length);
  });

  it('the number of ideas in any campaign should be greater than equal to 0', () => {
    const htmlElement: HTMLElement = fixture.nativeElement;
    const spanElement = htmlElement.querySelector('div.campaignBottomIcons div span.numbers');
    fixture.detectChanges();

    expect(spanElement.textContent).toBeGreaterThanOrEqual(0);
  });

  it('the campaign name should be defined', () => {
    const htmlElement: HTMLElement = fixture.nativeElement;
    const divElement = htmlElement.querySelector('div.campaignName');
    fixture.detectChanges();

    expect(divElement.textContent).toBeTruthy();
  });


});