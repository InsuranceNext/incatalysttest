import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, ActivatedRoute,NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized  } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams,HttpClientModule } from '@angular/common/http';

import { MatDatepickerModule,MatFormFieldModule,MatNativeDateModule} from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';

import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { CampaignsService } from '../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';
import { UploadService} from 'src/app/services/upload.service';

import { CreatecampaignComponent } from './createcampaign.component';

describe('CreatecampaignComponent', () => {
  let component: CreatecampaignComponent;
  let fixture: ComponentFixture<CreatecampaignComponent>;
  let authService:AuthService;
	
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:
      [
        RouterTestingModule,
        HttpClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatInputModule
      ],
      declarations: [ CreatecampaignComponent ],
      providers:[AuthService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const cognitoUser = {
    getSession: (callback) => {
      return null;
    },
    getUsername:()=> {
      return "User123";
    }
  };
  const authService = TestBed.get(AuthService);
  const authUserSpy = spyOn(authService, 'getAuthenticatedUser').and.returnValue(cognitoUser);
  fixture = TestBed.createComponent(CreatecampaignComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('Campaigns Created by Campaign Manager', () => {
	let component: CreatecampaignComponent;
	let fixture: ComponentFixture<CreatecampaignComponent>;
	let auth: AuthService;
	let uploadService: UploadService;
	let campaign:CampaignsService;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CreatecampaignComponent],
			providers: [AuthService]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CreatecampaignComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});


	it('should check for the user and get the token', () => {
		spyOn(auth, "getAuthenticatedUser");
		component.ngOnInit();
		expect(auth.getAuthenticatedUser).toHaveBeenCalled();
		expect(component.token).toBeDefined();
	});

	it('should download campaign image from S3 Bucket', () => {
		spyOn(uploadService, "downloadideadoc");
		let docurl = "http://s3.amazonaws.com/640343/test.xls";
		component.download(docurl);
		expect(uploadService.downloadideadoc).toHaveBeenCalled();
		expect(component.campaigndisplayimage).toBeDefined();
	});

	it('should set the Campaign Template to the passed parameter', ()=>{
		component.selectTemplate('BMC');
		expect(component.currentchoosetemplatepage).toBe('BMC');
	});

	it('should set the page title to the passed parameter', ()=>{
		component.setCurrentPage("Create Campaign");
		expect(component.campaignFormTitle).toBe("Create Campaign");
	});

	it('should add extra question tab for the Others Campaign', ()=>{
		component.addNewColumn();
		expect(component.checkForQuestion).toBe(false);
	});

	

	it('should upload Campaign Image to S3 bucket', ()=>{
		spyOn(uploadService,"uploadcampaigndoc");
		component.uploadimage();
		expect(uploadService.uploadcampaigndoc).toHaveBeenCalled();
	});

	it('should upload Campaign document to S3 bucket', ()=>{
		spyOn(uploadService,"uploadcampaigndoc");
		component.upload();
		expect(uploadService.uploadcampaigndoc).toHaveBeenCalled();
	});

	it('should create a Campaign', ()=>{
		spyOn(component,"saveCampaign");
		spyOn(campaign,"createCampaign");
		component.saveothertemplate();
		expect(component.saveCampaign).toHaveBeenCalled();
		expect(campaign.createCampaign).toHaveBeenCalled();
	})
});

