import { Component, OnInit, NgZone } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

import { CampaignsService } from 'src/app/services/campaigns.service';
import { OthersService } from 'src/app/services/others.service';
import { AuthService } from '../../services/auth/auth.service';
import { UploadService } from 'src/app/services/upload.service';


@Component({
    selector: 'app-createcampaign',
    templateUrl: './createcampaign.component.html',
    styleUrls: ['./createcampaign.component.css']
})
export class CreatecampaignComponent implements OnInit {

    _data: any;
    file: any;
    image: any;
    video:any;
    fileContent: any = '';
    fileContent1: any = '';
    fileContent2: any = '';
    fileContent3: any = '';
    videoContent: any = '';
    videoContent1: any = '';


    preSignedUrl: string = 'https://www.google.com';
    selectedFiles: any;
    selectedImages: any;
    campaignFormTitle: string = 'Create Campaign';
    campaignName: string = '';
    campaignStartDate: string = '';
    campaignEndDate: string = '';
    campaignDescription: string = '';
    selectedImage: any;
    selectedFile: any;
    problemOwner: string = '';
    campaignCategory: string = '';
    videoUrl: string = '';
    imageOption: string = 'defaultImage';
    videoOption:string='lessthan5mb';
    wizardActive: string = 'wizard-dot.svg';
    wizardInactive: string = 'wizard-dot-inactive.svg';
    wizard1: string = 'wizard-dot.svg';
    wizard2: string = 'wizard-dot-inactive.svg';
    wizard3: string = 'wizard-dot-inactive.svg';
    wizard4: string = 'wizard-dot-inactive.svg';
    questionArray = [{
        question: '',
        wordlimit: 50
    }];
    maxWords = [50, 100, 150];
    checkForQuestion = false;
    count = 0;
    emailIDs: string = '';
    getEmailIDs: string[];
    checkedAllUsers = false;
    other: any;
    defaultCampaignImage = true;
    token: any;
    campaigntemplate: any = {};
    currentchoosetemplatepage = 'Other';
    campaigndisplayimage:any;

    constructor(private route: Router, private campaign: CampaignsService, private router: ActivatedRoute, private zone: NgZone, private auth: AuthService, private others: OthersService, private uploadservice: UploadService) { }

    ngOnInit() {
        this.auth.getAuthenticatedUser().getSession((err, session) => {
            this.token = session.getIdToken().getJwtToken();
        });
        if (this.router.snapshot.paramMap.get('id')) {
            this.campaign.getCampaignById(this.router.snapshot.paramMap.get('id'), this.token).subscribe(data => {
                this.campaigntemplate = data[0];
                if(data[0].campaignimage)
                {
                this.defaultCampaignImage = false;
                this.download(data[0].campaignimage);
                var check=document.getElementById("checkboxUser") as HTMLInputElement;
                check.checked=true;

                }
                if(data[0].videourl){
                  if(data[0].videourl.startsWith("https://"))
                  {
                  var check=document.getElementById("videouser") as HTMLInputElement;
                  check.checked=true;
                  }
                }
                this.campaigntemplate.start_date = new Date(data[0].start_date);
                this.campaigntemplate.end_date = new Date(data[0].end_date);
                this.campaigntemplate.user = data[0].user._id;
                this.currentchoosetemplatepage = this.campaigntemplate.template_type;
                if(this.campaigntemplate.template_type=='Other')
                {
                this.others.getquestionbyid(data[0].other_id, this.token).subscribe(data => {
                    for (var i = 0; i < data[0].otherquestions.length; i++)
                        this.questionArray[i] = JSON.parse(data[0].otherquestions[i]);
                })
              }
            })
        }
        else {
            this.campaigntemplate.user = this.auth.getAuthenticatedUser().getUsername();
            this.campaigntemplate.category='';
            this.campaigntemplate.idea_ids = [];
            this.campaigntemplate.raters_email = [];
            this.campaigntemplate.template_type = 'Other';
        }
    }

    download(campaignimage) {

        this.uploadservice.downloadideadoc(campaignimage, this.token).subscribe((data: any) => {
          this.campaigndisplayimage=data;
        })

    }

    selectTemplate(getTemplate) {
        this.currentchoosetemplatepage = getTemplate;
        this.campaigntemplate.template_type = getTemplate;
    }

    setcurrentpage(page) {
        if (page === 'Create Campaign') {
            this.campaignFormTitle = 'Create Campaign';
        }

        if (page === 'Choose Template') {
            this.campaignFormTitle = 'Choose Template';
        }

        if (page === 'Launch Campaign') {
            this.campaignFormTitle = 'Launch Campaign';
        }

        if (page === 'Submit Campaign') {
            this.campaignFormTitle = 'Submit Campaign';
        }
    }



    addNewColumn() {
        var obj = {
            question: '',
            wordlimit: 50
        };
        this.questionArray.push(obj);
        this.checkForQuestion = false;
    }

    removeColumn() {
        this.questionArray.splice(-1, 1);
    }

    imageChange(e: any) {
        this.image = e.target.files[0];

        const fileReader: FileReader = new FileReader();
        const fileReader1: FileReader = new FileReader();

        fileReader.onloadend = (e) => {
            this.fileContent2 = fileReader.result;

        };
        fileReader1.onloadend = (e) => {
            this.fileContent1 = fileReader1.result;
        };
        fileReader.readAsArrayBuffer(this.image);
        fileReader1.readAsDataURL(this.image);
    }

    fileChange(e: any) {

        this.file = e.target.files[0];

        const fileReader: FileReader = new FileReader();
        const fileReader1: FileReader = new FileReader();

        fileReader.onloadend = (e) => {
            this.fileContent = fileReader.result;

        };

        fileReader1.onloadend = (e) => {
            this.fileContent3 = fileReader1.result;
        };


        fileReader.readAsArrayBuffer(this.file);
        fileReader1.readAsDataURL(this.file);
    }

    videoChange(e: any) {

        this.video = e.target.files[0];

        const fileReader: FileReader = new FileReader();
        const fileReader1: FileReader = new FileReader();

        fileReader.onloadend = (e) => {
            this.videoContent = fileReader.result;

        };

        fileReader1.onloadend = (e) => {
            this.videoContent1 = fileReader1.result;
        };


        fileReader.readAsArrayBuffer(this.video);
        fileReader1.readAsDataURL(this.video);
    }

    uploadimage() {
        this.defaultCampaignImage = false;
        if(this.image&&this.image.type.startsWith("image")){
        var filename = this.image.name;
        const base64File = this.fileContent1.split(',')[1];
        this.uploadservice.uploadcampaigndoc(base64File, filename, "image", this.token).subscribe((data: any) => {
            this.campaigntemplate.campaignimage = data.key;
            this.download(data.key);
        })
      }
      else{
        alert("Please Choose an Image to Upload");
      }

    }
    upload() {
      if((this.file)&&(this.file.name.endsWith("txt")||this.file.name.endsWith("doc")||this.file.name.endsWith("docx")||this.file.name.endsWith("pdf")||this.file.name.endsWith("ppt")||this.file.name.endsWith("pptx"))){
        var filename = this.file.name;
        if(this.file)
        {
        const base64File = this.fileContent3.split(',')[1];
        this.uploadservice.uploadcampaigndoc(base64File, filename, "file", this.token).subscribe((data: any) => {
            this.campaigntemplate.campaign_doc_url = data.key;
        })
      }
      else{
        alert("Please Choose a File before Upload");
      }
    }
    else if(!this.file){
      alert("Please Choose a File to Upload");
    }
    else{
      alert("Please Upload PDF,PPT or DOC Files");
    }
    }

    uploadvideo() {

     if(this.video&&this.video.type.startsWith("video")){
        var filename = this.video.name;
        if(this.video)
        {
        const base64File = this.videoContent1.split(',')[1];
        this.uploadservice.uploadcampaigndoc(base64File, filename, "video", this.token).subscribe((data: any) => {
            this.campaigntemplate.videourl = data.key;
        })
      }
      else{
        alert("Please Choose a File before Upload");
      }
    }
    else if(!this.video){
      alert("Please Choose a video to Upload");
    }
    else{
      alert("Please Upload a video");
    }
    }

    setCurrentPage(getPage) {
        if (getPage === 'Choose Template') {
            if (this.campaigntemplate.name !== ''&&this.campaigntemplate.name !== undefined) {
              if(this.campaigntemplate.description !== ''&&this.campaigntemplate.description !== undefined)
              {
                this.campaignFormTitle = 'Choose Template';
                this.wizard2 = this.wizardActive;
              }
              else{
                alert('Please enter the campaign Description');
              }

            } else {
                alert('Please enter the campaign name');
            }
        }
        if (getPage === 'Launch Campaign') {
            this.count = 0;
            // tslint:disable-next-line:prefer-for-of
            if(this.campaigntemplate.template_type=='Other')
            {
            for (let i = 0; i < this.questionArray.length; i++) {
                if (this.questionArray[i].question.length === 0) {
                    this.count++;
                }
            }
            if (this.count > 0) {
                this.checkForQuestion = true;
            } else {
                this.campaignFormTitle = 'Launch Campaign';
                this.wizard2 = this.wizardActive;
                this.wizard3 = this.wizardActive;
            }
          }
          else{
            this.campaignFormTitle = 'Launch Campaign';
            this.wizard2 = this.wizardActive;
            this.wizard3 = this.wizardActive;
          }
        }
        if (getPage === 'Submit Campaign') {
            if (this.campaigntemplate.allUsers || this.campaigntemplate.email_ids !== '') {
                this.campaignFormTitle = 'Submit Campaign';
                this.wizard2 = this.wizardActive;
                this.wizard3 = this.wizardActive;
                this.wizard4 = this.wizardActive;
            }
            else {
                alert('Fill the details');
            }
        }
    }

    selectWizard(getPage) {
        // On 1st page

        if (this.campaignFormTitle === 'Create Campaign' && this.campaigntemplate.name !== '') {
            if (getPage === this.campaignFormTitle) {
                // do nothing.
            }
            else {

            }
        }
        else {
            if (this.campaigntemplate.name === '') {
                alert('Please enter the campaign name');

            }
            else {
                // do nothing.
            }
        }

        // On 2nd page
        if (this.campaignFormTitle === 'Choose Template') {
            if (getPage === 'Choose Template') {
                // do nothing.
            }
            else {
                if (getPage === 'Create Campaign') {
                    this.campaignFormTitle = getPage;
                    this.wizard2 = this.wizardInactive;
                }
            }
        }

        // On 3rd page
        if (this.campaignFormTitle === 'Launch Campaign') {
            if (getPage === 'Launch Campaign') {
                // do nothing.
            }
            else {
                if (getPage === 'Create Campaign') {
                    this.campaignFormTitle = getPage;
                    this.wizard2 = this.wizardInactive;
                    this.wizard3 = this.wizardInactive;

                }
                if (getPage === 'Choose Template') {
                    this.campaignFormTitle = getPage;
                    this.wizard3 = this.wizardInactive;

                }
            }
        }

        // On 4th page
        if (this.campaignFormTitle === 'Submit Campaign') {
            if (getPage === 'Submit Campaign') {
                // do nothing.
            }
            else {
                if (getPage === 'Create Campaign') {
                    this.campaignFormTitle = getPage;
                    this.wizard2 = this.wizardInactive;
                    this.wizard3 = this.wizardInactive;
                    this.wizard4 = this.wizardInactive;

                }
                if (getPage === 'Choose Template') {
                    this.campaignFormTitle = getPage;
                    this.wizard3 = this.wizardInactive;
                    this.wizard4 = this.wizardInactive;

                }
                if (getPage === 'Launch Campaign') {
                    this.campaignFormTitle = getPage;
                    this.wizard4 = this.wizardInactive;

                }
            }

        }


    }
    saveothertemplate() {
        const others = {
            user: this.auth.getAuthenticatedUser().getUsername(),
            otherquestions: this.questionArray
        }

        this.others.save(others, this.token).subscribe(
            data => {
                this.other = data;
                this.saveCampaign();
            },
            error => {

            }

        );
    }

    saveCampaign() {
        var email = [];
        if (typeof(this.campaigntemplate.email_ids) == 'string') {

            if (this.campaigntemplate.email_ids.includes(",")) {

                this.campaigntemplate.email_ids.split(",").forEach(value => {
                    email.push(value);
                })
            } else {
                email[0] = this.campaigntemplate.email_ids;
            }
            this.campaigntemplate.email_ids = email;
        }


        // const createCampaignBody = {
        //   allUsers: this.checkedAllUsers,
        //   email_ids:this.getEmailIDs,
        //   campaign_doc_url: '',
        //   campaignimage: '',
        //   category: this.campaignCategory,
        //   description: this.campaignDescription,
        //   end_date: this.campaignEndDate,
        //   name: this.campaignName,
        //   other_id: '',
        //   problemowner: this.problemOwner,
        //   start_date: this.campaignStartDate,
        //   template_type: this.currentchoosetemplatepage,
        //   videourl: this.videoUrl,
        //   user:this.auth.getAuthenticatedUser().getUsername(),
        //   idea_ids:[],
        //   raters_email:[]
        // };
        if (this.currentchoosetemplatepage == 'Other') {
            this.campaigntemplate.other_id = this.other._id;
        }
        const self = this;
        this.campaign.createCampaign(this.campaigntemplate, this.token).subscribe(
            (data: any) => {

                if (this.currentchoosetemplatepage == 'Other') {
                    this.other.campaign_id = data._id;
                    this.updateother();
                } else {
                    this.route.navigate(['/ideation/mycampaigns'], {
                        relativeTo: this.router
                    });
                }
            },
            error => {}
        );
    }

    updateother() {
        this.others.save(this.other, this.token).subscribe(
            data => {
                this.route.navigate(['/ideation/mycampaigns'], {
                    relativeTo: this.router
                });
            },
            error => {}
        );
    }

}
