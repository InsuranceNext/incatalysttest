import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {MatDialogModule,MatDialogRef} from '@angular/material/dialog';
import { url } from 'inspector';
import { HttpClient, HttpHeaders, HttpParams,HttpClientModule } from '@angular/common/http';

import { UploadService} from 'src/app/services/upload.service';
import { CampaignsService } from '../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';
import { IdeasService } from './../../services/ideas.service';
import { UploadService} from 'src/app/services/upload.service';

import { MycampaignsComponent } from './mycampaigns.component';
import {  UrlpopupComponent } from './../urlpopup/urlpopup.component';

describe('MycampaignsComponent initialization', () => {
  let component: MycampaignsComponent;
  let campaignsservice:CampaignsService;
  let fixture: ComponentFixture<MycampaignsComponent>;
  let uploadService:UploadService;
  let authService:AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        RouterTestingModule,
        MatDialogModule,
        HttpClientModule
    ],
      declarations: [ MycampaignsComponent ],
      providers: [ UploadService,CampaignsService,AuthService ]
      })
      .compileComponents();
    }));

  beforeEach(() => {
    const cognitoUser = {
    getSession: (callback) => {
      return null;
    },
    getUsername:()=> {
      return "User123";
    }
  };
  const authService = TestBed.get(AuthService);
  const authUserSpy = spyOn(authService, 'getAuthenticatedUser').and.returnValue(cognitoUser);
  fixture = TestBed.createComponent(MycampaignsComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
  });
  it('should declare campaigns', () => {
    expect(component.campaigns).toBeDefined();
    });
  it('should declare showIcon', () => {
    expect(component.showIcon).toBe(false);
    });
  it('should create', () => {
    expect(component).toBeTruthy();
    });
});
describe('Given MycampaignsComponent is declared, When getting campaigns', () => {
  let campaignsservice:CampaignsService;
  let fixture: ComponentFixture<MycampaignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MycampaignsComponent ],
      providers: [ UploadService,CampaignsService ]
      })
      .compileComponents();
    }));
  it("should call getcampaigns function of campaigns service",()=>{
    spyon(campaignsservice,'getcampaigns');
    fixture = TestBed.createComponent(MycampaignsComponent);
    component = fixture.componentInstance;
    expect(component.download()).toHaveBeenCalled();
  })
});
describe('Given MycampaignsComponent is declared, When it is initialized', () => {
  let component: MycampaignsComponent;
  let fixture: ComponentFixture<MycampaignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MycampaignsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MycampaignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  var id;
  it('should set showIcon to true on clicking more icon', () => {
    component.show(id);
    expect(component.showIcon).toBe(true);
  });

  it('should set showId with parameter passed on clicking more icon', () => {
    component.show(id);
    expect(component.showId).toBe(id);
  });
  });
describe('Given MycampaignsComponent is declared, When getting report', () => {
  let component: MycampaignsComponent;
  let fixture: ComponentFixture<MycampaignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MycampaignsComponent ],
       providers: [ UploadService ]
    })
    .compileComponents();
    }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MycampaignsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    });
  var campaign;
  it('should get the url for downloading all ideas', () => {
    component.download(campaign);
    expect(component.url).not.toBeNull();
    });

  it('should get the url for downloading raters list', () => {
    component.ratedMembers();
    expect(component.url).not.toBeNull();
    });
  });
describe('Given MyCampaigns Component is declared,when it is initialized',()=>{
  let campaignsservice: CampaignsService;
  let authservice: AuthService;
  let uploadservice:UploadService;
  let matdia:MatDialog;
  let ideasservice:IdeasService;
  let router:Router;
  let route:ActivatedRouter;
  let http:HttpClient;
  let component:MycampaignsComponent;

  beforeEach(() => { (1)
    authservice = new AuthService();
    campaignsservice = new CampaignsService(http,authservice);
    ideasservice=new IdeasService(http);
    });
  it("should call getToken",()=>{
    spyOn(authservice,'getAuthenticatedUser');
    component=new component(campaignsservice,authservice,ideasservice,uploadservice,matdia,router,route);
    expect(component.getToken).toHaveBeenCalled();
  });
  it("should get Token that doesnot set the token variable",()=>{
    spyOn(authservice,'getAuthenticatedUser').and.throwError("Error");
  component=new component(campaignsservice,authservice,ideasservice,uploadservice,matdia,router,route);
  expect(component.getToken).toBeFalsy();
  });
  });

describe('Given MycampaignComponent is declared,when downloading ideas report',()=>{
  let campaignsservice: CampaignsService;
  let authservice: AuthService;
  let uploadservice:UploadService;
  let matdia:MatDialog;
  let ideasservice:IdeasService;
  let router:Router;
  let route:ActivatedRouter;
  let http:HttpClient;
  let component:MycampaignsComponent;
  beforeEach(() => {
    ideasservice=new IdeasService(http);
  });
  it('should call downloadAllIdeas functions with token',()=>{
    component=new component(campaignsservice,authservice,ideasservice,uploadservice,matdia,router,route);
    spyOn(ideasservice,'downloadAllIdeas');
    component.saveExcel();
    expect(ideasservice.downloadAllIdeas).toHaveBeenCalledWith(component.token);
  });
  it('should open the popup with url',()=>{
    component=new component(campaignsservice,authservice,ideasservice,uploadservice,matdia,router,route);
    spyOn(ideasservice,'downloadAllIdeas').and.returnValue("https://s3.amazonaws.com/670343/ideas.xls");
    component.saveExcel();
    expect(matdia.open).toHaveBeenCalled();
  });
  it('should return the error for downloading ideas',()=>{
    component=new component(campaignsservice,authservice,ideasservice,uploadservice,matdia,router,route);
    spyOn(ideasservice,'downloadAllIdeas').and.throwError("Error");
    component.saveExcel();
    expect(matdia.open).toBeFalsy();
  });
  });

describe('Given MycampaignComponent is declared,when downloading raters report',()=>{
  let campaignsservice: CampaignsService;
  let authservice: AuthService;
  let uploadservice:UploadService;
  let matdia:MatDialog;
  let ideasservice:IdeasService;
  let router:Router;
  let route:ActivatedRouter;
  let http:HttpClient;
  let component:MycampaignsComponent;
  beforeEach(() => {
    ideasservice=new IdeasService(http);
    authservice=new AuthService(http);
    component=new MycampaignsComponent(campaignsservice,authservice,ideasservice,uploadservice,matdia,router,route);
  });
  var userid;
  it('should invoke getAuthenticatedUser',()=>{
    spyOn(authservice,'getAuthenticatedUser');
    component.ratedMembers();
    expect(authservice,'getAuthenticatedUser').toHaveBeenCalled();
  });
  it('should call downloadRatedMembers function of ideaservice with token and userid',()=>{
    spyOn(ideasservice,'downloadRatedMembers');
    component.ratedMembers();
    expect(ideasservice.downloadRatedMembers).toHaveBeenCalledWith(userid,component.token);
  });
  it('should open popup with download url',()=>{
    spyOn(ideasservice,'downloadAllIdeas').and.returnValue("https://s3.amazonaws.com/670343/raters.xls");
    component.ratedMembers();
    expect(matdia.open).toHaveBeenCalled();
  });
  it('should return the error for downloading ideas',()=>{
    spyOn(ideasservice,'downloadRatedMembers').and.throwError("Error");
    component.ratedMembers();
    expect(matdia.open).not.toHaveBeenCalled();
  });
  });
describe('Given MyCampaignsComponent is initialized,when downloading private idea data',()=>{
  let campaignsservice: CampaignsService;
  let authservice: AuthService;
  let uploadservice:UploadService;
  let matdia:MatDialog;
  let ideasservice:IdeasService;
  let router:Router;
  let route:ActivatedRouter;
  let http:HttpClient;
  let component:MycampaignsComponent;
  beforeEach(() => {
    uploadservice=new UploadService(http,authservice);
    component=new component(campaignsservice,authservice,ideasservice,uploadservice,matdia,router,route);
  });
  var campaign:any={"name":"test","campaignimage":"/ins/img1"};
  it('it should call downloadideadoc function with two parameters',()=>{
    spyOn(uploadservice,'downloadideadoc');
    component.download(campaign);
    expect(uploadservice.downloadideadoc).toHaveBeenCalledWith(campaign.campaignimage,component.token);
  });
  it('should check if the campaign image field is available in campaign',()=>{
    expect(component.campaign.campaignimage).toBeDefined();
  });
  it('it should set url for private campaign image',()=>{
    spyOn(uploadservice,'downloadideadoc').and.returnValue("https://s3.amazonaws.com/670343/ideas.xls");
    component.download(campaign);
    expect(campaign.campaignimage).toEqual("https://s3.amazonaws.com/670343/ideas.xls");
  });
  it('it should return error',()=>{
    spyOn(uploadservice,'downloadideadoc').and.throwError("Error");
    component.download(campaign);
    expect(component.download).toBeFalsy();
  });
  });
