import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { CampaignsService } from '../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';
import { IdeasService } from './../../services/ideas.service';
import { UploadService} from 'src/app/services/upload.service';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {  UrlpopupComponent } from './../urlpopup/urlpopup.component';
import { url } from 'inspector';

@Component({
  selector: 'app-mycampaigns',
  templateUrl: './mycampaigns.component.html',
  styleUrls: ['./mycampaigns.component.css']
})
export class MycampaignsComponent implements OnInit {
  campaigns:any[]=[];
  showIcon:any=false;
  showId:any;
  token:any;
    constructor(private campaign: CampaignsService,private auth:AuthService,public dialog: MatDialog,private route: Router,private router:ActivatedRoute,private ideaservice: IdeasService,public uploadservice: UploadService) {

  }
  show(id){
    this.showIcon=true;
    this.showId=id;
  }


    ngOnInit() {
      this.auth.getAuthenticatedUser().getSession((err, session) => {
        this.token = session.getIdToken().getJwtToken();
           });

      this.campaign.getcampaigns(this.token).subscribe((data:any[]) => {

        if(data.length==0)
        {
        this.campaigns=[];

      }
        else
        {  data.forEach(campaign=>{
            this.download(campaign);

          })
        }

       }
     );
    }

    download(campaign) {
      if(campaign.campaignimage)
      {
        this.uploadservice.downloadideadoc(campaign.campaignimage, this.token).subscribe((data: any) => {
          campaign.campaignimage=data;
          this.campaigns.push(campaign);
          })
      }
      else{
        this.campaigns.push(campaign);
      }
    }

    goToshareideas(id){
      this.route.navigate([id+'/share'], { relativeTo: this.router });
    }
    editCampaign(template,campaign){
      this.route.navigate(['create/'+campaign._id], { relativeTo: this.router });
    }

    saveExcel(){
      this.ideaservice.downloadAllIdea(this.token).subscribe(url=>{

        let dialogRef = this.dialog.open( UrlpopupComponent, {
          width: '700px',
          disableClose: true,
          data:{"url":url}
        });
      })
    }

    ratedMembers(){
      var userid=this.auth.getAuthenticatedUser().getUsername();

      this.ideaservice.downloadRatedMembers(userid,this.token).subscribe(url=>{

        let dialogRef = this.dialog.open( UrlpopupComponent, {
          width: '700px',
          disableClose: true,
          data:{"url":url}
        });
      })
    }

}
