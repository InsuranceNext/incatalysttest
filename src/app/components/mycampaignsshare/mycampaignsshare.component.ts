import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { CampaignsService } from '../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';


@Component({
  selector: 'app-mycampaignsshare',
  templateUrl: './mycampaignsshare.component.html',
  styleUrls: ['./mycampaignsshare.component.css']
})
export class MycampaignsshareComponent implements OnInit {
  campaign: any;
  rater_email: any;
  message: any;
  token: any;
  constructor(private router: ActivatedRoute, private route: Router, private campaignservice: CampaignsService, private auth: AuthService) {
    this.campaign = [];
  }

  ngOnInit() {
    this.auth.getAuthenticatedUser().getSession((err, session) => {
      this.token = session.getIdToken().getJwtToken();
    });
    this.router.data.subscribe(data => {
      if (data.length == 0)
        this.campaign = [];
      else {
        this.campaign = data.campaign[0];

        this.rater_email = this.campaign.raters_email;

      }
    });
  }
  save() {
    var mailsendingusers = [];
    this.campaign.user = this.campaign.user._id;
     if (this.rater_email.includes(",")) {
        this.rater_email.split(",").forEach(value => {
          if (!this.campaign.raters_email.includes(value)) {
            this.campaign.raters_email.push(value);
            mailsendingusers.push(value);
          }
        })
      }
      else {
        if (!this.campaign.raters_email.includes(this.rater_email)) {
          this.campaign.raters_email.push(this.rater_email);
          mailsendingusers.push(this.rater_email);
        }
      }
      this.campaign.message = '';

      if (this.campaign._id) {
        this.campaignservice.updateforshare(this.campaign, mailsendingusers, this.token).subscribe(data => {
          this.route.navigate(['/ideation/mycampaigns'], { relativeTo: this.router });
        }
        );
      }
}

  cancel() {
    this.route.navigate(['/ideation/mycampaigns'], { relativeTo: this.router });
  }

}
