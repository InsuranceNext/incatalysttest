 import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MycampaignsshareComponent } from './mycampaignsshare.component';
import { CampaignsService } from '../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';

describe('Given MycampaignsshareComponent, When initializing', () => {
  let component: MycampaignsshareComponent;
  let fixture: ComponentFixture<MycampaignsshareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MycampaignsshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MycampaignsshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should declare campaign', () => {
    expect(component.campaign).toBeDefined();
  });
  it('should declare rater_email', () => {
    expect(component.rater_email).toBeDefined();
  });
  it('should declare token', () => {
    expect(component.token).toBeDefined();
  });
});

describe('Given MycampaignsshareComponent is declared, When it is sharing', () => {
  let component: MycampaignsshareComponent;
  let fixture: ComponentFixture<MycampaignsshareComponent>;
  let campaignsservice: CampaignsService;
  let authservice: AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MycampaignsshareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MycampaignsshareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authservice=new AuthService(http);
    campaignsservice=new CampaignsService(http,authservice);
    });
    let campaign:any={"name":"Campaign1","raters_email":['sangeetha@cognizant.com','saran@cognizant.com']};
    let rater_email:any="sng@cognizant.com,srn@cognizant.com";
    var mailsendingusers:any=[];
    it('should include , in rater_email incase shared for multiple users',()=>{
      component.save();
      let res=component.rater_email.includes(',');
      expect(res).toBe(true);
    });
    it('should populate campaign raters_email for sharing', () => {
      component.save();
      expect(component.campaign.raters_email).toBeGreaterThanOrEqual(1);
    });
    it('should set list of users to send email', () => {
      component.save();
      expect(mailsendingusers.length).toBeGreaterThanOrEqual(1);
    });


  let campaign:any={"name":"campaign1","campaignimage":"/campaign/image.jpg"};

  it('should call campaignsservice to update with three parameters',()=>{
	   spyOn(campaignsservice,'updateforshare');
     component.save();
	   expect(campaignsservice.updateforshare).toHaveBeenCalledWith(component.campaign,mailsendingusers,component.token);
    });
  it('should navigate to mycampaigns page on success',()=>{
	   spyOn(campaignsservice,'updateforshare').and.returnValue(campaign);
     component.save();
	   expect(router.navigate).toBeTruthy();
    });
  it('should return error on update',()=>{
	 spyOn(campaignsservice,'updateforshare').and.throwError("Error");
   component.save();
	 expect(component.save).toBeFalsy();
    });
});
