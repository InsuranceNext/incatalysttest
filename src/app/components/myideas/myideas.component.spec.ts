import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyideasComponent } from './myideas.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CampaignsService } from 'src/app/services/campaigns.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { IdeasService } from 'src/app/services/ideas.service';
import { UploadService } from 'src/app/services/upload.service';
import { Router, ActivatedRoute } from '@angular/router';

describe('Given MyideasComponent is initialized,', () => {
  let component: MyideasComponent;
  let fixture: ComponentFixture<MyideasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      declarations: [MyideasComponent],
      providers: [AuthService, { provide: CampaignsService, useValue: {} },
        IdeasService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyideasComponent);
    component = fixture.componentInstance;
    const cognitoUser = {
      getSession: (callback) => {
        return null;
      },
      getUsername: (callback) => {
        return 'name';
      }
    };

    const authService = TestBed.get(AuthService);
    const ideasService = TestBed.get(IdeasService);
    const authUserSpy1 = spyOn(authService, 'getAuthenticatedUser').and.returnValue(cognitoUser);
    // const authUserSpy2 = spyOn(ideasService, 'getideasforuser').and.returnValue(of('some value here'));
    fixture.detectChanges();
  });

  it('then there should be a component named MyideasComponent', () => {
    expect(component).toBeTruthy();
  });

  // it('then the readmore property should be false', () => {
  //   console.log(component.readmore);
  //   expect(component.readmore).toBeTruthy();
  // });

  it('then the property should have a property selectedtab', () => {
    // expect(component.selectedtab).toBe("idea");
    expect(component.selectedtab).toBeDefined();
  });

  it('then the component should have a myideas array', () => {
    expect(component.myIdeas).toBeDefined();
  });

  it('then the object display in the component should be defined', () => {
    expect(component.display).toBeDefined();
  });


  it('then the ideacampaign array should be defined', () => {
    expect(component.ideacampaign).toBeDefined();
  });

});



describe('Given that the toggle() method is called,', () => {
  let component: MyideasComponent;
  let fixture: ComponentFixture<MyideasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      declarations: [MyideasComponent],
      providers: [AuthService, { provide: CampaignsService, useValue: {} },
        IdeasService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(MyideasComponent);
    component = fixture.componentInstance;

    const cognitoUser = {
      getSession: (callback) => {
        return null;
      },
      getUsername: (callback) => {
        return 'name';
      }
    };

    const authService = TestBed.get(AuthService);
    const ideasService = TestBed.get(IdeasService);
    const authUserSpy1 = spyOn(authService, 'getAuthenticatedUser').and.returnValue(cognitoUser);
    component.toggle();


    fixture.detectChanges();
  });


  it('then the sizing property should be defined', () => {
    console.log(component.sizing);
    expect(component.sizing).not.toBeTruthy();
  });

});



describe('Given that displayIdea() method is called, ', () => {

  let component: MyideasComponent;
  let fixture: ComponentFixture<MyideasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MyideasComponent],
      providers: [AuthService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    let authservice: AuthService;
    
    fixture = TestBed.createComponent(MyideasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(authservice, 'getAuthenticatedUser').and.throwError('errrror');

    const mockIdea = {};

    component.displayIdea(mockIdea);
  });

  it('then, ideacampaign property should exist', () => {
    expect(component.ideacampaign).toBeTruthy();
  });

  it('then, display property should exist', () => {
    expect(component.display).toBeTruthy();
  });

});


describe('Given that displayIdea() method is called', () => {

  let http: HttpClient;
  let route: Router;
  let router: ActivatedRoute;
  const mockIdea = {};


  let ideasService: IdeasService;
  let authService: AuthService;
  let campaignsService: CampaignsService;
  let uploadService: UploadService;
  let component: MyideasComponent;

  beforeEach(() => {

    ideasService = new IdeasService(http);
    authService = new AuthService(http);
    campaignsService = new CampaignsService(http, authService);

    component = new MyideasComponent(ideasService, campaignsService, authService, route, router, uploadService);

  });

  it('then, ideacampaign property should exist', () => {
    spyOn(campaignsService, 'getCampaignById').and.returnValue(mockIdea);
    expect(component.ideacampaign).toBeTruthy();
    // expect(campaignsService.getCampaignById).toHaveBeenCalled();
  });

});




describe('Given that displayIdea() method is called', () => {

  let campaignsService: CampaignsService;
  const mockIdea = {};



  let component: MyideasComponent;
  let fixture: ComponentFixture<MyideasComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyideasComponent],
      providers: [IdeasService, AuthService, CampaignsService, UploadService, Router, ActivatedRoute]
    });
    fixture = TestBed.createComponent(MyideasComponent);
    component = fixture.componentInstance;
    campaignsService = TestBed.get(CampaignsService);

  });


  it('then, ideacampaign property should exist', () => {
    spyOn(campaignsService, 'getCampaignById').and.returnValue(mockIdea);
    expect(component.ideacampaign).toBeTruthy();
    expect(campaignsService.getCampaignById).toHaveBeenCalled();
  });

});






describe('Given that edit() method is called, ', () => {


  let routerService: Router;

  let component: MyideasComponent;
  let fixture: ComponentFixture<MyideasComponent>;

  beforeEach(() => {


    TestBed.configureTestingModule({
      declarations: [MyideasComponent],
      providers: [IdeasService, AuthService, CampaignsService, UploadService, Router, ActivatedRoute]
    });


    fixture = TestBed.createComponent(MyideasComponent);
    component = fixture.componentInstance;
    routerService = TestBed.get(Router);

    // component.edit(component.display._id, component.display.campaign_id);
  });

  it('then, router object should be defined.', () => {
    // tslint:disable-next-line:max-line-length
    spyOn(routerService, 'navigate').and.returnValue(['../campaigns/' + component.display._id + '/ideas/submit/' + component.display.campaign_id]);
    expect(routerService.navigate).toHaveBeenCalled();
  });

});



describe('Given that download() method is called, ', () => {

  let uploadService: UploadService;

  let component: MyideasComponent;
  let fixture: ComponentFixture<MyideasComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyideasComponent],
      providers: [IdeasService, AuthService, CampaignsService, UploadService, Router, ActivatedRoute]
    });

    fixture = TestBed.createComponent(MyideasComponent);
    component = fixture.componentInstance;
    uploadService = TestBed.get(UploadService);
  });

  it('then, the downloadideadoc() of UploadService should be called.', () => {

    spyOn(uploadService, 'downloadideadoc').and.returnValue(new Observable());
    expect(uploadService.downloadideadoc).toHaveBeenCalled();
  });

});


describe('Given that Idea tab is selected, ', () => {
  
  let component: MyideasComponent;
  let fixture: ComponentFixture<MyideasComponent>;
  
  
  beforeEach(() => {
    fixture = TestBed.createComponent(MyideasComponent);
    component = fixture.componentInstance;
  });


  it('then selectedtab property should contain the value idea', () => {
    expect(component.selectedtab).toEqual('idea');
  });

  it('then there must be a div in the DOM whose content should be Uploaded files  ', () => {
    const htmlElement: HTMLElement = fixture.nativeElement;
    const divElement = htmlElement.querySelector('div.row.uploadedfiles div.row.title');
    fixture.detectChanges();
    expect(divElement.textContent).toEqual('Uploaded files');
  });

  it('then there must be a div in the DOM whose content should be Other Details  ', () => {
    const htmlElement: HTMLElement = fixture.nativeElement;
    const divElement = htmlElement.querySelector('div.row.qa div.row.title');
    fixture.detectChanges();
    expect(divElement.textContent).toEqual('Other Details');
  });


});


describe('Given that Campaign Details tab is selected, ', () => {
  
  let component: MyideasComponent;
  let fixture: ComponentFixture<MyideasComponent>;
  
  
  beforeEach(() => {
    fixture = TestBed.createComponent(MyideasComponent);
    component = fixture.componentInstance;
  });


  it('then selectedtab property should contain the value campaigndetails', () => {
    expect(component.selectedtab).toEqual('campaigndetails');
  });

  it('then there must be a span in the DOM whose content should be ProblemOwner  ', () => {
    const htmlElement: HTMLElement = fixture.nativeElement;
    const spanElement = htmlElement.querySelector('div.problemowner span');
    fixture.detectChanges();
    expect(spanElement.textContent).toContain('ProblemOwner');
  });


});


describe('Given that My Idea page loads ', () => {
  let component: MyideasComponent;
  let fixture: ComponentFixture<MyideasComponent>;

  beforeEach(() => {
    fixture = TestBed.createComponent(MyideasComponent);
    component = fixture.componentInstance;
  });

  it('then, the number of ideas is greater than or equal to 0.', () => {
    expect(component.myIdeas).toBeGreaterThanOrEqual(0);
  });


});

describe('Given that ngOnInit() method is called, ', () => {

  let ideaService: IdeasService;
  let campaignservice: CampaignsService;
  let authservice: AuthService;
  let component: MyideasComponent;
  let fixture: ComponentFixture<MyideasComponent>;


  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyideasComponent],
      providers: [IdeasService, AuthService, CampaignsService, UploadService, Router, ActivatedRoute]
    });

    fixture = TestBed.createComponent(MyideasComponent);
    component = fixture.componentInstance;
    ideaService = TestBed.get(IdeasService);
    campaignservice = TestBed.get(CampaignsService);
    authservice = TestBed.get(AuthService);

    spyOn(authservice, 'getAuthenticatedUser').and.returnValue({});
    spyOn(ideaService, 'getideasforuser').and.returnValue([]);
    spyOn(campaignservice, 'getCampaignById').and.returnValue({});


    component.ngOnInit();

  });

  it('ideacampaign property should be defined', () => {
    expect(component.ideacampaign).toBeTruthy();
  });

  it('display property should be defined', () => {
    expect(component.display).toBeTruthy();
  });
});