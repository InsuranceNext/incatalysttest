import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { IdeasService } from './../../services/ideas.service';
import { AuthService } from '../../services/auth/auth.service';
import { CampaignsService } from '../../services/campaigns.service';
import { UploadService } from 'src/app/services/upload.service';

@Component({
  selector: 'app-myideas',
  templateUrl: './myideas.component.html',
  styleUrls: ['./myideas.component.css']
})
export class MyideasComponent implements OnInit {

  myIdeas: any = [];
  ideacampaign: any;
  display: any;
  togglebool: boolean = true;
  selectedtab: String = "idea";
  readmore: boolean = false;
  sizing: String = "maximize";
  token: any;
  constructor(private ideaservice: IdeasService, private campaignservice: CampaignsService, private auth: AuthService, private route: Router, private router: ActivatedRoute, private uploadservice: UploadService) { }

  ngOnInit() {
    this.auth.getAuthenticatedUser().getSession((err, session) => {
      this.token = session.getIdToken().getJwtToken();
    });

    this.ideaservice.getideasforuser(this.auth.getAuthenticatedUser().getUsername(), this.token).subscribe((data: any[]) => {
      this.myIdeas = data;
      if (data.length != 0) {
        this.campaignservice.getCampaignById(data[0].campaign_id, this.token).subscribe((campaign: any) => {
          this.ideacampaign = campaign;
          this.display = this.myIdeas[0];
          this.display.questions = JSON.parse(this.display.questions);
          this.display.answers = JSON.parse(this.display.answers);
        });
      }
    });
  }

  edit(ideaid, campaignid) {
    this.route.navigate(['../campaigns/' + campaignid + '/ideas/submit/' + ideaid], { relativeTo: this.router });
  }

  displayIdea(idea) {
    this.campaignservice.getCampaignById(idea.campaign_id, this.token).subscribe(campaign => {
      this.ideacampaign = campaign;
      this.display = idea;
      this.display.questions = typeof (this.display.questions) == "string" ? JSON.parse(this.display.questions) : this.display.questions;
      this.display.answers = typeof (this.display.answers) == "string" ? JSON.parse(this.display.answers) : this.display.answers;
    })

  }
  toggle() {
    this.togglebool = !this.togglebool;
    if (this.togglebool === false) {
      this.sizing = 'minimize';
    } else {
      this.sizing = 'maximize';
    }
  }

  download(ideaurl) {
    this.uploadservice.downloadideadoc(ideaurl, this.token).subscribe((data: any) => {
      window.open(data);
    })
  }

}
