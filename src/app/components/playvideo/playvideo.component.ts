import { Component, OnInit } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-playvideo',
  templateUrl: './playvideo.component.html',
  styleUrls: ['./playvideo.component.css']
})
export class PlayvideoComponent implements OnInit {
videourlpopup:any;
videourl:any;
campaignname:any;
  constructor(public dialogRef: MatDialogRef<PlayvideoComponent>,@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.campaignname=this.data.campaignname;
    this.videourl=this.data.url;
    
  }
  close(){
    this.dialogRef.close();
  }
}
