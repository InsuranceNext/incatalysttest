import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayvideoComponent } from './playvideo.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

describe('Given PlayvideoComponent is initialized,', () => {
  let component: PlayvideoComponent;
  let fixture: ComponentFixture<PlayvideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayvideoComponent ],
      providers: [{ provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayvideoComponent);
    component = fixture.componentInstance;

    component.videourl = 'dummyUrl';

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('then there should be a property videourl', () => {
    expect(component.videourl).toBeTruthy();
  });

  it('should be a property campaignname', () => {
    expect(component.campaignname).toBeDefined();
  });
});
