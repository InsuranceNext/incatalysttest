import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {MatDialogModule,MatDialogRef} from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { IdeapopupService } from '../../services/ideapopup.service';

import { IdeapopupComponent } from './ideapopup.component';

describe('IdeapopupComponent', () => {
  const mockDialogRef = {
    close: jasmine.createSpy('close')
  };
  let component: IdeapopupComponent;
  let fixture: ComponentFixture<IdeapopupComponent>;
  let ideapopupService: IdeapopupService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    imports:[
    FormsModule,
    MatDialogModule,
    ],
    declarations: [ IdeapopupComponent ],
    providers:[IdeapopupService,{
          provide: MatDialogRef,
          useValue: mockDialogRef
        }]
    })
    .compileComponents();
  }));
var idea={
  "user":"91265jsdfwgeyjkliio3244",
  "answers":{
    Founder: [],
    Customer: [],
    ValueProposition: [],
    Channel: [],
    KeyActivities: [],
    Partners: [],
    KeyResources: [],
    CostStructure: [],
    RevenueModel: [],
  }
}
var questionarray: any = [{
            "Customer": [{
                "question": "Which classes are you creating values for?\nWho is your most important customer?"
            }]
        }, {
            "Founder": [{
                "question": "What relationship that the target customer expects you to establish?\nHow can you integrate that into your business in terms of cost and format?"
            }]
        }, {
            "ValueProposition": [{
                "question": " What core value do you deliver to the customer?\nWhich customer needs are you satisfying?"
            }]
        }, {
            "Channel": [{
                "question": "Through which channels that your customers want to be reached?\nWhich channels work best? How much do they cost? How can they be integrated into your and your customers routines?"
            }]
        }, {
            "KeyActivities": [{
                "question": "What key activities does your value proposition require?\nWhat activities are important the most in distribution channels, customer relationships, revenue stream?"
            }]
        }, {
            "Partners": [{
                "question": "Who are your key partners/suppliers?.\nWhat are the motivations for the partnerships?"
            }]
        }, {
            "KeyResources": [{
                "question": "What key resources does your value proposition require?\nWhat resources are important the most in distribution channels, customer relationships, revenue stream?"
            }]
        }, {
            "CostStructure": [{
                "question": "What are the most cost in your business?\nWhich key resources/ activities are most expensive?"
            }]
        }, {
            "RevenueModel": [{
                "question": "For what value are your customers willing to pay?\nWhat and how do they recently pay? How would they prefer to pay?\nHow much does every revenue stream contribute to the overall revenues?"
            }]
        }
];
  beforeEach(() => {
    ideapopupService=TestBed.get(IdeapopupService);
  });

  it('should create', () => {
    spyOn(ideapopupService,'checkservice');
  ideapopupService.checkservice("founders",idea,questionarray);
  var sp=spyOn(IdeapopupComponent.prototype,'ngOnInit')
  fixture = TestBed.createComponent(IdeapopupComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});

describe('List of BMC or LMC questions in poup', () => {
	let component: IdeapopupComponent;
	let fixture: ComponentFixture<IdeapopupComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [IdeapopupComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(IdeapopupComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});


	it('idea should be defined', ()=>{
		component.ngOnInit();
		expect(component.idea).toBeDefined();
	})
});

