import { Component, OnInit ,Inject} from '@angular/core';
import { IdeapopupService } from '../../services/ideapopup.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-ideapopup',
  templateUrl: './ideapopup.component.html',
  styleUrls: ['./ideapopup.component.css']
})
export class IdeapopupComponent implements OnInit {

    solutiondescription= "";
    founder="";
    valueproposition="";
    keyactivities="";
    keyresources="";
    channel="";
    partners="";
    coststructure="";
    revenuemodel="";
    counter = 0;
    customervalue: any;
    foundervalue: any;
    valuepropositionvalue: any;
    keyactivitiesvalue: any;
    keyresourcesvalue: any;
    coststructurevalue: any;
    revenuemodelvalue: any;
    channelvalue: any;
    partnersvalue: any;
    questionarray:any;
    currentbmcpage:any;
    idea:any={};


  constructor( public dialogRef: MatDialogRef<IdeapopupComponent>,private ideaPopUpService:IdeapopupService) { }

        ngOnInit() {
          this.idea=this.ideaPopUpService.idea;
          
          this.questionarray = this.ideaPopUpService.questionarray;
          this.currentbmcpage = this.ideaPopUpService.currentbmcpage;
          this.idea.questions={};
          for(var i=0;i<this.questionarray.length;i++)
          {

            var key=Object.keys(this.questionarray[i])[0];

            var val=Object.values(this.questionarray[i])[0][0].question;
            this.idea.questions[key]={};

            //this.idea.answers[key]={};
            this.idea.questions[key][0]=val;
          }


        }


          setbmcpage(bmcpagename)
         {
            if(this.currentbmcpage=="customersandrelationships")
             {
              if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('Customer'))
              this.ideaPopUpService.customer(this.idea.answers.Customer[this.counter]);
                }
              } else
                 this.ideaPopUpService.customer("");
          }
             if(this.currentbmcpage=="channel")
                {
                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('Channel'))
              this.ideaPopUpService.channel(this.idea.answers.Channel[this.counter]);
                } } else
                 this.ideaPopUpService.channel("");
             }

              if(this.currentbmcpage=="partners")
                {
                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('Partners'))
              this.ideaPopUpService.partners(this.idea.answers.Partners[this.counter]);
                } } else
                 this.ideaPopUpService.partners("");
             }

             if(this.currentbmcpage=="keyactivities")

                {
                 if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('KeyActivities'))
              this.ideaPopUpService.keyactivities(this.idea.answers.KeyActivities[this.counter]);
                } } else
                 this.ideaPopUpService.keyactivities("");
             }

             if(this.currentbmcpage=="keyresources"){

                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('KeyResources'))
              this.ideaPopUpService.keyresources(this.idea.answers.KeyResources[this.counter]);
                } } else
                 this.ideaPopUpService.keyresources("");

             }
              if(this.currentbmcpage=="valueproposition"){

                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('ValueProposition'))
              this.ideaPopUpService.valueproposition(this.idea.answers.ValueProposition[this.counter]);
                } } else
                 this.ideaPopUpService.valueproposition("");
              }
              if(this.currentbmcpage=="founders") {
                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('Founder'))
              this.ideaPopUpService.founders(this.idea.answers.Founder[this.counter*4]);
                } } else
                 this.ideaPopUpService.founders("");
              }
             if(this.currentbmcpage=="coststructure"){

                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('CostStructure'))
              this.ideaPopUpService.coststructure(this.idea.answers.CostStructure[this.counter]);
                } } else
                 this.ideaPopUpService.coststructure("");

             }
              if(this.currentbmcpage=="revenuemodel") {
                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('RevenueModel'))
              this.ideaPopUpService.revenuemodel(this.idea.answers.RevenueModel[this.counter]);
                } } else
                 this.ideaPopUpService.revenuemodel("");
              }
              this.currentbmcpage = bmcpagename;
         }

        ngDoCheck(){
          this.checkwizard();
        }

          checkwizard() {

              this.customervalue = this.ideaPopUpService.customervalue;
              this.foundervalue = this.ideaPopUpService.foundervalue;
              this.valuepropositionvalue = this.ideaPopUpService.valuepropositionvalue;
              this.keyactivitiesvalue = this.ideaPopUpService.keyactivitiesvalue;
              this.keyresourcesvalue = this.ideaPopUpService.keyresourcesvalue;
              this.coststructurevalue = this.ideaPopUpService.coststructurevalue;
              this.revenuemodelvalue = this.ideaPopUpService.revenuemodelvalue;
              this.channelvalue = this.ideaPopUpService.channelvalue;
              this.partnersvalue = this.ideaPopUpService.partnersvalue;
          }

          close()
         {

              if(this.currentbmcpage=="customersandrelationships")
             {
               if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('Customer'))
              this.ideaPopUpService.customer(this.idea.answers.Customer[this.counter]);
                }
              } else
                 this.ideaPopUpService.customer("");
          }
             if(this.currentbmcpage=="channel")
                {
               if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('Channel'))
              this.ideaPopUpService.channel(this.idea.answers.Channel[this.counter]);
                } } else
                 this.ideaPopUpService.channel("");
             }

              if(this.currentbmcpage=="partners")
                {
                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('Partners'))
              this.ideaPopUpService.partners(this.idea.answers.Partners[this.counter]);
                } } else
                 this.ideaPopUpService.partners("");
             }

             if(this.currentbmcpage=="keyactivities")

                {

                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('KeyActivities'))
              this.ideaPopUpService.keyactivities(this.idea.answers.KeyActivities[this.counter]);
                } } else
                 this.ideaPopUpService.keyactivities("");
             }

             if(this.currentbmcpage=="keyresources"){

                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('KeyResources'))
              this.ideaPopUpService.keyresources(this.idea.answers.KeyResources[this.counter]);
                } } else
                 this.ideaPopUpService.keyresources("");

             }
              if(this.currentbmcpage=="valueproposition"){

                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('ValueProposition'))
              this.ideaPopUpService.valueproposition(this.idea.answers.ValueProposition[this.counter]);
                } } else
                 this.ideaPopUpService.valueproposition("");
              }
              if(this.currentbmcpage=="founders") {
                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('Founder'))
              this.ideaPopUpService.founders(this.idea.answers.Founder[this.counter*4]);
                } } else
                 this.ideaPopUpService.founders("");
              }
             if(this.currentbmcpage=="coststructure"){

                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('CostStructure'))
              this.ideaPopUpService.coststructure(this.idea.answers.CostStructure[this.counter]);
                } } else
                 this.ideaPopUpService.coststructure("");

             }
              if(this.currentbmcpage=="revenuemodel") {
                if(this.idea.hasOwnProperty('answers')){
                    { if(this.idea.answers.hasOwnProperty('RevenueModel'))
              this.ideaPopUpService.revenuemodel(this.idea.answers.RevenueModel[this.counter]);
                } } else
                 this.ideaPopUpService.revenuemodel("");
              }

              this.dialogRef.close({data:this.idea});
           //  $modalInstance.close({idea: this.idea, questionarray:this.questionarray});

         }











}
