import { Component, OnInit } from '@angular/core';
import { CampaignsService } from '../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';
import { UploadService} from 'src/app/services/upload.service';

import { Router} from '@angular/router';
import { ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-campaigns',
  templateUrl: './campaigns.component.html',
  styleUrls: ['./campaigns.component.css']
})

export class CampaignsComponent implements OnInit {
campaigns:any;
showIcon:any=false;
showId:any;
categorychoosen:any=false;
countInsurance:any;
countCMT:any;
countother:any;
countHealth:any;
countTotal:any;
token:any;
categorylist=[{"displayname":'Insurance',
 "description":"Business statements from Insurance domain on Artifical Intelligence and Analytics",
 "image":"insurance.png",
 "name":"Insurance"
 },
 {"displayname":'Communications Media & Technology',
 "description":"Business statements from Communications Media & Technology domain on Artifical Intelligence and Analytics",
 "image":"CMT.png",
"name":"CMT"},
 {"displayname":'Healthcare',
 "description":"Business statements from Healthcare domain on Artifical Intelligence and Analytics",
 "image":"Healthcare.png",
"name":"Health Care"},
{"displayname":'Others',
"description":"Business statements from Digital Business domain on Artifical Intelligence and Analytics",
"image":"Default-image-square.svg",
"name":"Others"}
 ];
constructor(private campaign: CampaignsService,private auth:AuthService,private uploadservice:UploadService,private route:Router,private router:ActivatedRoute) {}
show(id){
  this.showIcon=true;
  this.showId=id;
}
selectcategory(category){
  this.categorychoosen=category;
}
categorySeperation(campaigns){
 this.countInsurance = 0;
 this.countCMT = 0;
 this.countother = 0;
 this.countHealth = 0;
 this.countTotal = 0;
   for(var i=0; i<campaigns.length; i++){
       if(campaigns[i].category == "Insurance")
       this.countInsurance++;
     else if(campaigns[i].category == "CMT" )
       this.countCMT++;
     else if(campaigns[i].category == "Health Care" )
       this.countHealth++;
    else
         this.countother++;
   }
   this.countTotal = this.countInsurance + this.countCMT + this.countother + this.countHealth;
}

download(campaign) {
  if(campaign.campaignimage)
  {
    this.uploadservice.downloadideadoc(campaign.campaignimage, this.token).subscribe((data: any) => {
      campaign.campaignimage=data;
      this.campaigns.push(campaign);
      })
  }
  else{
    this.campaigns.push(campaign);
  }
}

  ngOnInit() {
    this.auth.getAuthenticatedUser().getSession((err, session) => {
      this.token = session.getIdToken().getJwtToken();
         });
         this.auth.getcurrentuserfromdb().subscribe((data:any)=>{
              this.campaign.getcampaignsbyuser(data[0].email,this.token).subscribe((data:any[]) => {
                if(data.length==0)
                {
                this.campaigns=[];
                }
                else
                {
                  this.campaigns=[];
                  this.categorySeperation(data);
                  data.forEach(campaign=>{
                    this.download(campaign);
                  })

                }
                });
           })
  }
}
