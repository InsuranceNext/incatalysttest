import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignsComponent } from './campaigns.component';
import { CampaignsService } from '../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';
import { UploadService } from 'src/app/services/upload.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { Router, ActivatedRoute, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { of } from 'rxjs';

describe('CampaignsComponent', () => {
	let component: CampaignsComponent;
	let campaign: CampaignsService;
	let auth: AuthService;
	let fixture: ComponentFixture<CampaignsComponent>;
	const defaultToken = 'abcd1234';

	const cognitoUser = {
		getSession: (callback) => {
			return null;
		},
		getUserName: 'test'
	};

	const forms = {
		subscribe: () => { }
	}

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CampaignsComponent],
			providers: [AuthService, { provide: CampaignsService, userValue: {} }],
			imports: [RouterTestingModule, HttpClientModule],
		})
			.compileComponents();


		auth = TestBed.get(AuthService);
		let spyAuth = spyOn(auth, "getAuthenticatedUser").and.returnValue(cognitoUser);
		let spyuser = spyOn(auth, "getcurrentuserfromdb").and.returnValue(forms);
		fixture = TestBed.createComponent(CampaignsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	}));

	it('should create', () => {
		expect(component).toBeTruthy();
	});


	it('should check for the user and get the token', () => {
		expect(auth.getAuthenticatedUser).toHaveBeenCalled();
	});

	it('should get the logged in users data from getcurrentuserfromdb service', () => {

		expect(auth.getcurrentuserfromdb).toHaveBeenCalled();
	});


});

describe('List of available campaigns created by Campaign Manager', () => {
	let component: CampaignsComponent;
	let campaign: CampaignsService;
	let auth: AuthService;
	let upload: UploadService;
	let fixture: ComponentFixture<CampaignsComponent>;

	const cognitoUser = {
		getSession: (callback) => {
			return null;
		},
		getUserName: 'test'
	};

	const forms = {
		subscribe: () => { "hi" }
	}

	const mockCampaign = {
		
	}

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CampaignsComponent],
			providers: [AuthService, UploadService, { provide: CampaignsService, userValue: {subscribe: () => { return [{"name":"hi"}] }} }],
			imports: [RouterTestingModule, HttpClientModule],
		})
			.compileComponents();
	}));

	beforeEach(() => {
		auth = TestBed.get(AuthService);
		upload = TestBed.get(UploadService);
		
		let spyAuth = spyOn(auth, "getAuthenticatedUser").and.returnValue(cognitoUser);
		let spyuser = spyOn(auth, "getcurrentuserfromdb").and.returnValue(forms);
		let spydownloadImage=spyOn(upload, "downloadideadoc").and.returnValue(forms);
		
		fixture = TestBed.createComponent(CampaignsComponent);
		component = fixture.componentInstance;

	});

	
	it('ShowId should be defined', () => {
		expect(component.showId).toBeUndefined();
	});

	it('campaigns should be defined', () => {
		expect(component.campaigns).toBeDefined();
	});

	it('should set ShowId to passed argument', () => {
		component.show("234");
		expect(component.showId).toEqual("234");
	});

	it('should set categorychoosen to passed argument', () => {
		component.selectcategory("BMC");
		expect(component.categorychoosen).toEqual("BMC");
	});

	it('should upload campaign Image', () => {
		let campaigns = { "campaignimage": "bvsdvcv" };
		component.download(campaigns);
		expect(upload.downloadideadoc).toHaveBeenCalled();
	});

	it('should upload campaign Image and push the url to campaing', () => {
		let campaigns = { "campaignimage": "bvsdvcv" };
		component.download(campaigns);
		expect(component.campaigns.campaignimage).toEqual("hi");
	});

	it('should set ShowId to passed argument', () => {
		component.show("234");
		expect(component.showIcon).toEqual(true);
	});

	it('should set categorychoosen to passed argument', () => {
		component.selectcategory("BMC");
		expect(component.categorychoosen).not.toEqual("");
	});
});


describe('given categoryseperation have been called', () => {
	let component: CampaignsComponent;
	let campaign: CampaignsService;
	let auth: AuthService;
	let upload: UploadService;
	let fixture: ComponentFixture<CampaignsComponent>;
	let campaigns = [{ "category": "Insurance" }, { "category": "CMT" }, { "category": "CMT" }, { "category": "Others" }, { "name": "No category", "category":"Others" }];

	const cognitoUser = {
		getSession: (callback) => {
			return null;
		},
		getUserName: 'test'
	};

	const forms = {
		subscribe: () => { "hi" }
	}

	beforeEach(() => {
		TestBed.configureTestingModule({
			declarations: [CampaignsComponent],
			providers: [AuthService, UploadService],
			imports: [RouterTestingModule, HttpClientModule],
		})
			.compileComponents();
		auth = TestBed.get(AuthService);
		let spyAuth = spyOn(auth, "getAuthenticatedUser").and.returnValue(cognitoUser);
		fixture = TestBed.createComponent(CampaignsComponent);
		component = fixture.componentInstance;
		component.categorySeperation(campaigns);
	});

	it('and retun the Insurance count 1', () => {
		expect(component.countInsurance).toEqual(1);
	});

	it('and retun the CMT count 2', () => {
		expect(component.countInsurance).toEqual(1);
	});

	it('and retun the Others count 2', () => {
		expect(component.countInsurance).toEqual(2);
	});

	it('and retun the total count 4', () => {
		expect(component.countInsurance).toEqual(1);
	});


});

