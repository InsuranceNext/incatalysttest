import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-incatalysthome',
  templateUrl: './incatalysthome.component.html',
  styleUrls: ['./incatalysthome.component.css']
})
export class IncatalysthomeComponent implements OnInit {

   constructor(private router : Router) {}

  ngOnInit() { }

}
