import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { IdeasService } from '../../services/ideas.service';
import { CampaignsService } from '../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';
import { UploadService} from 'src/app/services/upload.service';


import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {  UrlpopupComponent } from './../urlpopup/urlpopup.component';

@Component({
  selector: 'app-mycampaignsviewideas',
  templateUrl: './mycampaignsviewideas.component.html',
  styleUrls: ['./mycampaignsviewideas.component.css']
})
export class MycampaignsviewideasComponent implements OnInit {
  ideas:any;
  campaign:any;
  showSpecificIdea:any=0;
  display:any;
  token:any;
  currentIdeaNo:any=0;

    constructor(private idea: IdeasService,private router : ActivatedRoute,public dialog: MatDialog,private route: Router,private auth:AuthService,private uploadservice:UploadService) {

  }

  displayIdea(idea,index) {

    this.currentIdeaNo=index+1;
      if(index<this.ideas.length)
      {
       this.showSpecificIdea=1;
        this.display=this.ideas.find(function(element) {
          return element._id==idea;
        });
      this.display.questions=typeof(this.display.questions)=="string"?JSON.parse(this.display.questions):this.display.questions;
      this.display.answers=typeof(this.display.answers)=="string"?JSON.parse(this.display.answers):this.display.answers;
      }
      }
  goToListIdeas(){
    this.showSpecificIdea=!this.showSpecificIdea;
  }
  goToshareideas(){
    this.route.navigate(['../../../../mycampaigns',this.campaign._id,'share'], { relativeTo: this.router });
  }

  saveExcel(){

    this.idea.downloadCampaignIdea(this.campaign._id,this.campaign.template_type,this.campaign.name,this.token).subscribe(url=>{

      let dialogRef = this.dialog.open( UrlpopupComponent, {
        width: '700px',
        disableClose: true,
        data:{"url":url}
      });
    })
  }
  download(docurl) {
      this.uploadservice.downloadideadoc(docurl, this.token).subscribe((data: any) => {
          window.open(data);

      })
  }


  nextIdea(currentIdeaNO){

   if(currentIdeaNO<this.ideas.length)
   this.displayIdea(this.ideas[currentIdeaNO]._id,currentIdeaNO);
 }
  previousIdea(currentIdeaNO){

   if(currentIdeaNO<=this.ideas.length&&(currentIdeaNO-2)>=0)
   this.displayIdea(this.ideas[currentIdeaNO-2]._id,currentIdeaNO-2);
 }
    ngOnInit() {
      this.auth.getAuthenticatedUser().getSession((err, session) => {
        this.token = session.getIdToken().getJwtToken();
      });
            this.router.data.subscribe(data =>
            {
              this.ideas=data.ideas;
              this.campaign=data.campaign[0];
                if(this.campaign.campaignimage)
              this.uploadservice.downloadideadoc(this.campaign.campaignimage, this.token).subscribe((data: any) => {
                this.campaign.campaignimage=data;
              });

            });

    }
}
