import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MycampaignsviewideasComponent } from './mycampaignsviewideas.component';

describe('MycampaignsviewideasComponent', () => {
  let component: MycampaignsviewideasComponent;
  let fixture: ComponentFixture<MycampaignsviewideasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MycampaignsviewideasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MycampaignsviewideasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
