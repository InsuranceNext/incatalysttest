import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, ActivatedRoute,NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized  } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams,HttpClientModule } from '@angular/common/http';

import { AuthService } from '../../services/auth/auth.service';

import { IdeationComponent } from './ideation.component';

describe('Given IdeationComponent is declared', () => {
  let component: IdeationComponent;
  let fixture: ComponentFixture<IdeationComponent>;
  let authService:AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        RouterTestingModule,
        HttpClientModule
      ],
      declarations: [ IdeationComponent ],
      providers:[AuthService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const authService = TestBed.get(AuthService);
    const authUserdb = spyOn(authService, 'getcurrentuserfromdb').and.returnValue({ subscribe: () => {} });
    fixture = TestBed.createComponent(IdeationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should define currentaccord', () => {
    expect(component.currentaccord).toBeDefined();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
describe('Given IdeationComponent is declared, When it is initialized', () => {
  let component: IdeationComponent;
  let fixture: ComponentFixture<IdeationComponent>;
  let router:Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        RouterTestingModule.withRoutes([]),
        HttpClientModule
      ],
      declarations: [ IdeationComponent ],
      providers:[AuthService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const authService = TestBed.get(AuthService);
    const authUserdb = spyOn(authService, 'getcurrentuserfromdb').and.returnValue({ subscribe: () => {} });
    fixture = TestBed.createComponent(IdeationComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router)
    fixture.detectChanges();
  });

  it('should set currentaccord on choosing an accord', () => {
    var pagename="campaigns";
    spyOn(component,'currentaccord');
    component.gotoselectedpage(pagename);
    expect(component.currentaccord).toBe(pagename);
  });
  it('should navigate to selected page on choosing an accord', () => {
    let navigatespy=spyOn(router,'navigate');
    var pagename="campaigns";
    spyOn(component,'currentaccord');
    component.gotoselectedpage(pagename);
    expect(navigatespy).toHaveBeenCalled();
  });


});
