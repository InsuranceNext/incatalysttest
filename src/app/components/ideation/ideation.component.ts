import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-ideation',
  templateUrl: './ideation.component.html',
  styleUrls: ['./ideation.component.css']
})
export class IdeationComponent implements OnInit {
currentaccord:String;
authentication:any;
  constructor(private route : Router,private router : ActivatedRoute,private auth:AuthService) {
    this.route.events.forEach((event) => {
        if(event instanceof NavigationStart) {
          var url=event.url;

          var accord=url.slice(url.indexOf("ideation/")+9);
          if(accord.indexOf('/')>=0)
            this.currentaccord=accord.substring(0,accord.indexOf('/'));
          else
            this.currentaccord=accord;
        }
      });
}

  ngOnInit() {
    this.auth.getcurrentuserfromdb().subscribe((data:any)=>{

      this.authentication=data[0];

    });

    var url=this.route.url;
    var accord=url.slice(url.indexOf("ideation/")+9);
    if(accord.indexOf('/')>=0)
      this.currentaccord=accord.substring(0,accord.indexOf('/'));
    else
      this.currentaccord=accord;

  }
goToIdeation(){

}
gotoselectedpage(pagename){
  this.currentaccord=pagename;
  this.route.navigate(['/ideation/'+pagename], { relativeTo: this.router });
}
}
