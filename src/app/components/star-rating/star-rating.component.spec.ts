import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarRatingComponent } from './star-rating.component';

describe('StarRatingComponent', () => {
  let component: StarRatingComponent;
  let fixture: ComponentFixture<StarRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('Score should be not defined', () => {
    expect(component.score).toBeUndefined();
  });
  it('MaxScore should be defined', () => {
    expect(component.maxScore).toBeDefined();
  });
  it('MaxScore should contain value 5', () => {
    expect(component.maxScore).toBeTruthy();
  });
  it('ForDisplay should be defined', () => {
    expect(component.forDisplay).toBeDefined();
  });
  it('range should be defined', () => {
    expect(component.range).toBeDefined();
  });
  it('Marked should be defined', () => {
    expect(component.marked).toBeDefined();
  });
  it('Marked should contain value -1', () => {
    expect(component.marked).toBeTruthy();
  });
  it('mark function should be defined', () => {
    expect(component.mark).toBeDefined();
  });
  it('isMarked function should be defined', () => {
    expect(component.isMarked).toBeDefined();
  });
  it('should display stars based on score', () => {
    expect(component.isMarked(0)).toBeTruthy();
  });


});
