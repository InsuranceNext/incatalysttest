import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlpopupComponent } from './urlpopup.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

describe('UrlpopupComponent', () => {
  let component: UrlpopupComponent;
  let fixture: ComponentFixture<UrlpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UrlpopupComponent],
      providers: [{ provide: MatDialogRef, useValue: {} },
      { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('downloadUrl should be defined', () => {
    expect(component.downloadUrl).toBeDefined();
  });

  it('should close Popup window', () => {
    // component.close();
    // expect(component.dialogRef.close).toHaveBeenCalled();

    const spy = spyOn(component.dialogRef, 'close');
    component.close();
    expect(spy).toHaveBeenCalled();

  });

});
