import { Component, OnInit } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-urlpopup',
  templateUrl: './urlpopup.component.html',
  styleUrls: ['./urlpopup.component.css']
})

export class UrlpopupComponent implements OnInit {
downloadUrl:any;

  constructor(public dialogRef: MatDialogRef<UrlpopupComponent>,@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

    this.downloadUrl=this.data.url;
  }


close(){
  this.dialogRef.close();
}
}
