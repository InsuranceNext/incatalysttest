import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, ActivatedRoute,NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized  } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams,HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';

import { CampaignsService } from '../../services/campaigns.service';
import { AuthService } from '../../services/auth/auth.service';
import { UploadService} from 'src/app/services/upload.service';
import { IdeasService } from './../../services/ideas.service';
import { RatingsService } from './../../services/ratings.service';

import { StarRatingComponent } from './../../components/star-rating/star-rating.component';
import { DashboardComponent } from './dashboard.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let authService:AuthService;
  let ideasService:IdeasService;
  let campaignsService: CampaignsService;
  let uploadService: UploadService;
  let ratingsService:RatingsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        HttpClientModule
      ],
      declarations: [ DashboardComponent,StarRatingComponent ],
      providers:[
        CampaignsService,
        IdeasService,
        AuthService,
        UploadService,
        RatingsService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    const cognitoUser = {
    getSession: (callback) => {
      return null;
    },
    getUsername:()=> {
      return "User123";
    }
  };
  const authService = TestBed.get(AuthService);
  const authUserSpy = spyOn(authService, 'getAuthenticatedUser').and.returnValue(cognitoUser);
  fixture = TestBed.createComponent(DashboardComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

describe('Dashboard containing Number of campaigns,ideas and users details', () => {
	let component: DashboardComponent;
	let fixture: ComponentFixture<DashboardComponent>;
	let auth: AuthService;
	let ratingservice:RatingsService;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DashboardComponent],
			providers: [AuthService, RatingsService]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DashboardComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});



	it('should check for the user authentiaction and valid token', ()=>{
		spyOn(auth, "getAuthenticatedUser");
		component.ngOnInit();
		expect(auth.getAuthenticatedUser).toHaveBeenCalled();
		expect(component.token).toBeDefined();
	});

	it('should draw chart for the latest campaign', ()=>{
		spyOn(component,"drawchart");
		component.nextcampaign();
		expect(component.drawchart).toHaveBeenCalled();
	});

	it('should get the latest rated idea detail', ()=>{
		spyOn(ratingservice,"getratingsforcampaigns");
		component.ngOnInit();
		expect(ratingservice.getratingsforcampaigns).toHaveBeenCalled();
	})
});
