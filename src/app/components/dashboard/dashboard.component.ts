import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CampaignsService } from '../../services/campaigns.service';
import { IdeasService } from './../../services/ideas.service';
import { RatingsService } from './../../services/ratings.service';
import { AuthService } from '../../services/auth/auth.service';
import { UploadService} from 'src/app/services/upload.service';

import { DatePipe } from '@angular/common';
import {Chart} from 'chart.js';

import { StarRatingComponent } from './../../components/star-rating/star-rating.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  providers: [DatePipe],
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    ideas:any;
    campaignname:any;
    latestidea:any={};
    ideacampaignname:any=[];
    avgrate:any;
    ratername:any;
    rate:any;
    ratingcreateddate:any;
    campaignids:any=[];
    campaigns:any=[];
    ideasid:any=[];
    numberofusers:any=0;
    totalcampaigns:any=0;
    totalideas:any=0;
    mytotalideas:any=0;
    campaignchartname:any;
    latestideacampaignname:any='';
    latestideacampaignimage:any='';
    ratedIdeaId:any;
    ratings:any;
    myideas:any;
    ratedIdeaName:any;
    user:any;
    token:any;
    jsonfile:any = {
      "jsonarray": []
      };
      z:any=-1;


  constructor(private httpClient: HttpClient,private campaign: CampaignsService,private ideaservice: IdeasService,private ratingsService:RatingsService,private datePipe: DatePipe,private auth:AuthService,private uploadservice:UploadService) {
      this.user=this.auth.getAuthenticatedUser().getUsername();
  }

  ngOnInit() {
    this.auth.getAuthenticatedUser().getSession((err, session) => {
      this.token = session.getIdToken().getJwtToken();
      if(this.token)  {
         this.campaign.getcampaigns(this.token).subscribe((result:any[])=>{
         this.campaigns = result;

         this.totalcampaigns=result.length;

         let latestdate;
         for(let i=0;i<result.length;i++){
            this.campaignids[i]=result[i]._id;
            }

        if(this.campaignids.length!=0)
        {
          //number of users
          this.ideaservice.getIdeasforCampaignidArray(this.campaignids,this.token).subscribe((ideas:any[]) =>{
          this.ideas = ideas;

          this.totalideas = ideas.length;

          for(let i=0;i<ideas.length;i++){
      		if(ideas[i].user==null)
      		continue;
          this.ideasid[i]=ideas[i].user._id;
          }
          for(let i=0;i<this.ideasid.length;i++){
          for(let j=i+1;j<this.ideasid.length;j++){
            if(this.ideasid[i]==this.ideasid[j])
            {
              this.ideasid[j]='';
            }
          }
        }
        let count=0;
        for(let i=0;i<this.ideasid.length;i++){
          if(this.ideasid[i]!='')
		        ++count;
          }

          this.numberofusers=count;

          this.latestidea=ideas[0];
          var self=this;
          this.campaigns.find(function(element) {
            if(element._id == ideas[0].campaign_id)
            {
              self.latestideacampaignname=element.name;
              if(element.campaignimage){
                self.download(element.campaignimage);
              }
              else{
                self.latestideacampaignimage="/assets/img/Default-image-square.svg";
              }

            }
          });
        });
      }
           this.getRatingsOverview();
    });

      this.ideaservice.getideasforuser(this.user,this.token).subscribe((result:any[]) =>{

            this.myideas=result;
        this.mytotalideas=result.length;
    });
    }
  });
  this.nextcampaign();
 }
 download(campaignimage) {

     this.uploadservice.downloadideadoc(campaignimage, this.token).subscribe((data: any) => {
       this.latestideacampaignimage=data;

       })

 }

  nextcampaign(){
          this.campaign.getcampaigns(this.token).subscribe((campaign:any[])=>{
           if(this.z<(campaign.length)-1)
           {
             for(let i=0;i<=this.jsonfile.jsonarray.length;i++)
             this.jsonfile.jsonarray.pop();
             this.z++;
           this.campaignchartname=campaign[this.z].name;
           this.ideaservice.getideasforcampaign(campaign[this.z]._id,this.token).subscribe((result:any[]) =>{
             if(result.length!=0)
             {
               let chartdate= this.datePipe.transform(result[0].created,"MMMM dd");
               let cnt=1;
               for(let i=1;i<result.length;i++){
                 let chartdate1= this.datePipe.transform(result[i].created,"MMMM dd");
                 if(chartdate!=chartdate1)
                 {
                 this.jsonfile.jsonarray.push({'date':chartdate,'value':cnt});
                 chartdate=this.datePipe.transform(result[i].created,"MMMM dd");
                  cnt=1;
                  }
                  else if(chartdate==chartdate1)
                  {
                  cnt++;
                  }
                }
              this.jsonfile.jsonarray.push({'date':chartdate,'value':cnt});
              this.push();

            }
            else if(result.length==0)
            {
              this.push();
            }
            });
          }
        });

}

 prevcampaign(){
        this.campaign.getcampaigns(this.token).subscribe((campaign:any[])=>{
           for(let i=0;i<=this.jsonfile.jsonarray.length;i++)
           this.jsonfile.jsonarray.pop();
           if(this.z<=campaign.length && this.z>0)
           {
             this.z=this.z-1;
            this.campaignchartname=campaign[this.z].name;
            this.ideaservice.getideasforcampaign(campaign[this.z]._id,this.token).subscribe((result:any[]) =>{
             if(result.length!=0)
             {
               let chartdate=this.datePipe.transform(result[0].created,"MMMM dd");
               let cnt=1;
               for(let i=1;i<result.length;i++){
                 let chartdate1=this.datePipe.transform(result[i].created,"MMMM dd");
                 if(chartdate!=chartdate1)
                 {
                  this.jsonfile.jsonarray.push({'date':chartdate,'value':cnt});
                 chartdate=this.datePipe.transform(result[i].created,"MMMM dd");
                 cnt=1;
                  }
                  else if(chartdate==chartdate1)
                  {
                    cnt++;
                  }
                }
                this.jsonfile.jsonarray.push({'date':chartdate,'value':cnt});

              this.drawchart();
              }
            else if(result.length==0)
            {
              this.drawchart();
            }
            });
          }
        });
}

getRatingsOverview(){
  if(this.campaignids.length!=0)
    {
      let n;
       this.ratingsService.getratingsforcampaigns(this.campaignids,this.token).subscribe((result:any[]) => {
        this.ratings=result;
        if(result[0]!=undefined)
        {
        this.ratedIdeaId=result[0].idea_id;
        this.ideaservice.getideabyid(this.ratedIdeaId,this.token).subscribe((ideationResult:any) =>{
        this.ratedIdeaName=ideationResult[0].name;
        });
        if(result.length!=0)
        {

            this.ratername=result[0].user.displayName;
            this.rate=result[0].rate;
            // let floorValue=Math.floor(this.rate);
            // let floatValue=result[0].rate-floorValue;
            // if (floatValue>0.4)
            // {
            // this.rate=Math.ceil(this.rate[0]);
            // }
            // else
            // {
            // this.rate=Math.floor(this.rate[0]);
            // }
            this.ratingsService.getidearating(this.ratedIdeaId,this.token).subscribe((avgrate:any[])=>{
            var sum=0;
            for(var i=0;i<avgrate.length;i++)
            {
              sum=sum+avgrate[i].rate;
            }
            this.avgrate=sum/avgrate.length;

          });

            }
            this.ratingcreateddate=result[0].created;
          }
      });
    }
}

push(){
      let labels = this.jsonfile.jsonarray.map(function(e) {
         return e.date;
      });
      let data = this.jsonfile.jsonarray.map(function(e) {
         return e.value;
      });
      this.drawchart();
    }

  drawchart(){

            let labels = this.jsonfile.jsonarray.map(function(e) {
            return e.date;
            });
            let data = this.jsonfile.jsonarray.map(function(e) {
              return e.value;
            });
            // Global Options
            let config = {
              type:'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
              data:{
                labels:labels,
                datasets:[{
                  data:data,
                  backgroundColor:'#6eb43f',
                  }
                ]},
                options:{
                  legend:{
                    display:false
                    },
                    tooltips:{
                      enabled:true
                    },
                    scales: {
                      xAxes: [{
                        ticks:{
                          fontSize:10,
                          fontFamily:"Roboto",
                          fontColor:"#999999"
                          },
                      gridLines:{display:false,
                        drawBorder:true,
                        color:'rgba(66, 152, 181, 0.4)',},
                        barPercentage:0.2
                        }],
                    yAxes: [{
                      gridLines: {
                        display:true,
                        drawBorder: false,
                        color:'rgba(66, 152, 181, 0.2)',
                      },
                      ticks: {
                        fontSize:10,
                        fontColor:"#999999",
                        fontFamily:"Roboto",
                        stepSize: 2,
                        max: 10,
                      },
                      scaleLabel: {
                        display: true,
                        labelString: 'IDEAS',
                        fontSize:10,
                        fontColor:"#999999",
                      }
                    }]
                  },
                  chartArea: {
                    backgroundColor: 'rgba(0, 0, 0, 0.5)'
                  }
                }
              };
            let massPopChart = new Chart("myChart",config);
            Chart.defaults.scale.ticks.beginAtZero = true;



}



}
