import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingcampaignideaComponent } from './ratingcampaignidea.component';
import { StarRatingComponent } from '../star-rating/star-rating.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CampaignsService } from 'src/app/services/campaigns.service';
import { IdeasService } from 'src/app/services/ideas.service';
import { of } from 'rxjs';
import { UploadService } from 'src/app/services/upload.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RatingsService } from 'src/app/services/ratings.service';

describe('Given RatingcampaignideaComponent is initialized', () => {
  let component: RatingcampaignideaComponent;
  let fixture: ComponentFixture<RatingcampaignideaComponent>;



  beforeEach(async(() => {



    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      declarations: [RatingcampaignideaComponent,
        StarRatingComponent,
      ],
      providers: [AuthService,
        { provide: CampaignsService, useValue: {} },
        IdeasService
        // { provide: IdeasService, useValue: useIdeasService },

      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingcampaignideaComponent);
    component = fixture.componentInstance;


    const cognitoUser = {
      getSession: (callback) => {
        return null;
      },
    };

    component.campaign = {};
    component.campaign._id = 123;
    component.campaign.name = [];
    component.campaign.idea_ids = [];

    const authService = TestBed.get(AuthService);
    const ideasService = TestBed.get(IdeasService);
    const authUserSpy1 = spyOn(authService, 'getAuthenticatedUser').and.returnValue(cognitoUser);
    const authUserSpy2 = spyOn(ideasService, 'getideasforcampaign').and.returnValue(of('some value here'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('then there should be a property token', () => {
    expect(component.token).toBeTruthy();
  });

  it('then there should be a property campaign', () => {
    expect(component.campaign).toBeTruthy();
  });

  it('then there should be a property desc', () => {
    expect(component.desc).toBeTruthy();
  });

  it('then there should be a property ideas', () => {
    expect(component.ideas).toBeTruthy();
  });

  it('then there should be a property ratings', () => {
    expect(component.ratings).toBeTruthy();
  });

  it('then there should be a property score', () => {
    expect(component.score).toBeTruthy();
  });

  it('then there should be a property idealistname', () => {
    expect(component.idealistname).toBeTruthy();
  });

  it('then there should be a property newidealist', () => {
    expect(component.newidealist.length).toBeGreaterThanOrEqual(0);
  });

  it('then there should be a property ratedidealist', () => {
    expect(component.ratedidealist.length).toBeGreaterThanOrEqual(0);
  });

  it('then there should be a property display', () => {
    expect(component.display).toBeTruthy();
  });

  it('then there should be a property currentIdeaNo', () => {
    expect(component.currentIdeaNo).toBeTruthy();
  });

  it('then there should be a property showSpecificIdea', () => {
    expect(component.showSpecificIdea).toBeTruthy();
  });

  it('then there should be a property selectedtabname', () => {
    expect(component.selectedtabname).toBeTruthy();
  });

  it('then there should be a property togglebool', () => {
    expect(component.togglebool).toBeTruthy();
  });
});


describe('Given init() method is called ', () => {

  let ideaService: IdeasService;
  let component: RatingcampaignideaComponent;
  let fixture: ComponentFixture<RatingcampaignideaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RatingcampaignideaComponent],
      providers: [AuthService, UploadService, Router, ActivatedRoute, RatingsService, IdeasService, CampaignsService]
    });

    fixture = TestBed.createComponent(RatingcampaignideaComponent);
    component = fixture.componentInstance;
    ideaService = TestBed.get(IdeasService);

  });

  it('then, getideasforcampaign() of UploadService should be called.', () => {
    spyOn(ideaService, 'getideasforcampaign').and.returnValue({});
    expect(ideaService.getideasforcampaign).toHaveBeenCalled();
  });

  it('then, ideas property should exist', () => {
    spyOn(ideaService, 'getideasforcampaign').and.returnValue({});
    expect(component.ideas).toBeTruthy();
  });


});

describe('Given that goToListIdeas() method is called, ', () => {
  let component: RatingcampaignideaComponent;
  let fixture: ComponentFixture<RatingcampaignideaComponent>;
  let getBoolean;

  beforeEach(() => {

    fixture = TestBed.createComponent(RatingcampaignideaComponent);
    component = fixture.componentInstance;
    component.goToListIdeas();
    getBoolean = component.showSpecificIdea;
  });


  it('then the showSpecificIdea property value is complemented', () => {
    expect(component.showSpecificIdea).toEqual(!getBoolean);
  });

  it('then the length of newidealist array should be 0', () => {
    expect(component.newidealist.length).toEqual(0);
  });

  it('then the length of ratedidealist array should be 0', () => {
    expect(component.ratedidealist.length).toEqual(0);
  });

});


describe('Given that calculateavgrate() method is called, ', () => {

  let ratingService: RatingsService;
  let component: RatingcampaignideaComponent;
  let fixture: ComponentFixture<RatingcampaignideaComponent>;

  let idea = {};
  let index = 0;
  let list = 'new';
  let ideaRating = [];
  let newidealistLength = 0;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RatingcampaignideaComponent],
      providers: [AuthService, UploadService, Router, ActivatedRoute, RatingsService, IdeasService, CampaignsService]
    });

    fixture = TestBed.createComponent(RatingcampaignideaComponent);
    component = fixture.componentInstance;
    ratingService = TestBed.get(RatingsService);

    newidealistLength = component.newidealist.length;


  });

  it('length of newidealist array should be incremented by 1', () => {
    spyOn(ratingService, 'getuserrating').and.returnValue(ideaRating);

    component.calculateavgrate(idea, list, index);

    expect(component.newidealist.length).toEqual(newidealistLength + 1);

  });

});

describe('Given that calculateavgrate() method is called, ', () => {

  let ratingService: RatingsService;
  let component: RatingcampaignideaComponent;
  let fixture: ComponentFixture<RatingcampaignideaComponent>;

  let idea = {};
  let index = 0;
  let list = 'rated';
  let ideaRating = [];
  let ratedidealistLength = 0;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RatingcampaignideaComponent],
      providers: [AuthService, UploadService, Router, ActivatedRoute, RatingsService, IdeasService, CampaignsService]
    });

    fixture = TestBed.createComponent(RatingcampaignideaComponent);
    component = fixture.componentInstance;
    ratingService = TestBed.get(RatingsService);

    ratedidealistLength = component.newidealist.length;


  });

  it('length of ratedidealist array should be incremented by 1', () => {
    spyOn(ratingService, 'getuserrating').and.returnValue(ideaRating);

    component.calculateavgrate(idea, list, index);

    expect(component.ratedidealist.length).toEqual(ratedidealistLength + 1);

  });

});


describe('Given that displayIdea() method is called, ', () => {
  let component: RatingcampaignideaComponent;
  let fixture: ComponentFixture<RatingcampaignideaComponent>;

  let idea = {};
  let index = 0;
  let type = 'rated';
  let currentIdeaNo = 0;

  beforeEach(() => {

    fixture = TestBed.createComponent(RatingcampaignideaComponent);
    component = fixture.componentInstance;

    currentIdeaNo = component.currentIdeaNo;

    component.displayIdea(idea, index, type);
  });

  it('idealistname property should be updated', () => {
    expect(component.idealistname).toEqual(type);
  });

  it('if index value is less than ideas property array length, then currentIdeaNo property should be incremented', () => {
    expect(component.currentIdeaNo).toEqual(currentIdeaNo + 1);
  });

  it('enableidea property should be updated.', () => {
    expect(component.enableidea).toEqual(idea);
  });

  it('showSpecificIdea property should be updated.', () => {
    expect(component.showSpecificIdea).toBeTruthy();
  });

});


describe('Given that one of the campaign is selected, ', () => {
  let component: RatingcampaignideaComponent;
  let fixture: ComponentFixture<RatingcampaignideaComponent>;
  let tabType;


  beforeEach(() => {
    fixture = TestBed.createComponent(RatingcampaignideaComponent);
    component = fixture.componentInstance;
    tabType = component.selectedtabname;
  });

  it('then the number of ideas should show alongside the tab.', () => {

    const htmlElement: HTMLElement = fixture.nativeElement;
    const divElement = htmlElement.querySelector('div.row.tophat div');
    fixture.detectChanges();

    if (tabType === 'new') {
      expect(divElement.textContent).toContain(component.newidealist.length);
    } else {
      expect(divElement.textContent).toContain(component.ratedidealist.length);
    }
  });
});


describe('Given that ngOnInit() is called, ', () => {


  let authService: AuthService;
  let uploadService: UploadService;
  let component: RatingcampaignideaComponent;
  let fixture: ComponentFixture<RatingcampaignideaComponent>;


  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RatingcampaignideaComponent],
      providers: [AuthService, UploadService, Router, ActivatedRoute, RatingsService, IdeasService, CampaignsService]
    });

    fixture = TestBed.createComponent(RatingcampaignideaComponent);
    component = fixture.componentInstance;
    authService = TestBed.get(AuthService);
    uploadService = TestBed.get(UploadService);

    spyOn(authService, 'getAuthenticatedUser').and.returnValue({});
    spyOn(uploadService, 'downloadideadoc').and.returnValue({});

    component.ngOnInit();
  });

  it('then campaignid property should be defined', () => {
    expect(component.campaignid).toBeDefined();
  });

  it('then showSpecificIdea property should have a value of 0', () => {
    expect(component.showSpecificIdea).toEqual(0);
  });
});