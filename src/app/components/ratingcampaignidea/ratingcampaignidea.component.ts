import { Component, OnInit, NgModule, EventEmitter, Input, Output } from '@angular/core';

import { CampaignsService } from './../../services/campaigns.service';
import { IdeasService } from './../../services/ideas.service';
import { RatingsService } from './../../services/ratings.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../../services/auth/auth.service';
import { UploadService } from 'src/app/services/upload.service';

import { StarRatingComponent } from './../../components/star-rating/star-rating.component';

@Component({
    selector: 'app-ratingcampaignidea',
    templateUrl: './ratingcampaignidea.component.html',
    styleUrls: ['./ratingcampaignidea.component.css']
})
export class RatingcampaignideaComponent implements OnInit {
    campaign: any;
    desc: any = '';
    togglebool: boolean = true;
    showSpecificIdea: any = 0;
    selectedtabname: String = 'new';
    campaignid: any;
    ideas: any;
    avgrate: any;
    avgratenew: any;
    idearate: any;
    ideasid: any = [];
    display: any;
    idearatings: any;
    enableidea: any;
    rate: any;
    rating_star: any = {};
    currentIdeaNo: any = 0;
    avgratedisplay: any;
    token: any;
    ratings: any = {};
    score: number = 0;
    displayRatingScore = 4;
    newidealist: any = [];
    ratedidealist: any = [];
    idealistname: any;
    constructor(private campaignservice: CampaignsService,
                private ideaservice: IdeasService,
                private ratingsService: RatingsService,
                private router: Router, private route: ActivatedRoute,
                private auth: AuthService, private uploadservice: UploadService) { }
    onRateChange = (score) => {
        this.score = score;
        this.getSelectedRating(this.display, score);
    }
    ngOnInit() {
        this.auth.getAuthenticatedUser().getSession((err, session) => {
            this.token = session.getIdToken().getJwtToken();
        });
        this.campaignid = this.route.snapshot.paramMap.get('id');
        this.showSpecificIdea = 0;
        this.route.data.subscribe(data => {
            this.campaign = data.campaign[0];
            this.uploadservice.downloadideadoc(this.campaign.campaignimage, this.token).subscribe((data: any) => {
                this.campaign.campaignimage = data;
            });
        });
        this.init();
    }

    init() {
        this.ideaservice.getideasforcampaign(this.campaign._id, this.token).subscribe(data => {
            this.ideas = data;
            this.display = this.ideas[0] || [];
            this.getUserRating(this.ideas[0], 0);
        });
    }

    goToListIdeas() {
        this.showSpecificIdea = !this.showSpecificIdea;
        this.newidealist = [];
        this.ratedidealist = [];
        this.init();
    }
    nextIdea(currentIdeaNO) {
        if (currentIdeaNO < this.ideas.length)
            this.displayIdea(this.ideas[currentIdeaNO], currentIdeaNO, this.idealistname);
    }
    previousIdea(currentIdeaNO) {

        if (currentIdeaNO < this.ideas.length)
            this.displayIdea(this.ideas[currentIdeaNO - 2].idea, currentIdeaNO - 2, this.idealistname);
    }

    getUserRating(idea: any, index) {
        if (index < this.ideas.length) {
            this.ratingsService.getuserrating(idea._id, this.auth.getAuthenticatedUser().getUsername(), this.token).subscribe((rating_star: any[]) => {
                if (rating_star.length != 0) {
                    idea.rate = rating_star[0].rate;
                    this.calculateavgrate(idea, "rated", index);
                } else {
                    idea.rate = 0;
                    this.calculateavgrate(idea, "new", index);
                }
            });
        }
        else { }
    }

    calculateavgrate(idea, list, index) {
        this.ratingsService.getidearating(idea._id, this.token).subscribe((idearating: any[]) => {
            var sum = 0;
            for (var i = 0; i < idearating.length; i++) {
                sum = sum + idearating[i].rate;
            }
            idea.avgrate = sum / idearating.length;
            if (list == "new")
                this.newidealist.push(idea);
            else
                this.ratedidealist.push(idea);
            this.getUserRating(this.ideas[index + 1], index + 1)
        });
    }

    displayIdea(idea: any, index: any, ideatype) {
        this.idealistname = ideatype;
        if (index < this.ideas.length)
            this.currentIdeaNo = index + 1;
        this.enableidea = idea;
        this.showSpecificIdea = 1;
        if (ideatype == 'new')
            this.display = this.newidealist[index];
        else
            this.display = this.ratedidealist[index];
        for (var i = 0; i < this.display.questions.length; i++) {

            if (typeof (this.display.questions) == 'string')
                this.display.questions = JSON.parse(this.display.questions);
            if (typeof (this.display.answers) == 'string')
                this.display.answers = JSON.parse(this.display.answers);

        }
        this.display.template_type = 'Other';
    }


    getSelectedRating(display, rating) {
        this.ratingsService.getuserrating(display._id, this.auth.getAuthenticatedUser().getUsername(), this.token).subscribe((data: any[]) => {
            if (data.length != 0) {
                this.ratings = data[0];
                this.ratings.rate = rating;
            }
            else {
                this.ratings.rate = rating;
            }
            this.saveRating(display);
        });
    }

    saveRating(idea) {
        this.ratings.campaign_id = idea.campaign_id;
        this.ratings.idea_id = idea._id;
        this.ratings.user = this.auth.getAuthenticatedUser().getUsername();
        this.ratingsService.saverating(this.ratings, this.token).subscribe((data: any) => {
            this.score = 0;
            this.display.rate = data.rate;
            var x = document.getElementById("snackbar");
            x.className = "show";
            setTimeout(function () {
                x.className = x.className.replace("show", "");
            }, 1000);
        })
    }




}
