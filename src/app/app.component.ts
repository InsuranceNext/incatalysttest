import { Component,ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute,NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized  } from '@angular/router';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'INCatalyst7';
  hamburger:boolean=false;
  header:any;
  user:any;
  authentication:any;
  firstpath:any;
  displayname:any;
  secondpath:any;
  split:any;
  constructor(private router: Router, private route: ActivatedRoute,private auth: AuthService,private ref: ChangeDetectorRef) {
    this.authentication=this.auth.getAuthenticatedUser();

    //||this.router.url!='signin'||this.router.url!='signup'
    if(this.router.url!='/')
    {

     this.auth.getcurrentuserfromdb().subscribe((data:any)=>{

       this.user=data[0];

     });
   }
   this.router.events.forEach((event) => {
       if(event instanceof NavigationStart) {


        this.split=event.url.split('/');

        if(this.split.length==3){
        this.firstpath=this.split[2].charAt(0).toUpperCase()+ this.split[2].slice(1);
        this.displayname=this.split[2].charAt(0).toUpperCase()+this.split[2].slice(1);
        this.secondpath="";
        }
        else if(this.split.length>3){

          this.displayname=this.split[2].charAt(0).toUpperCase()+this.split[2].slice(1);
          this.firstpath=this.split[2].charAt(0).toUpperCase()+ this.split[2].slice(1);
        if(this.split[3].indexOf('-')>=0){
        this.secondpath=this.split[4].charAt(0).toUpperCase()+this.split[4].slice(1);
        }
        else{
        this.secondpath=this.split[3].charAt(0).toUpperCase()+this.split[3].slice(1);
        }
        }


         if(event.url=='/signin'||event.url=='/signup'||event.url=='/')
         {

         }
         else{

           this.auth.getcurrentuserfromdb().subscribe((data:any)=>{

             this.user=data[0];

           });
         }
       }
     });

}

  onCreateCampaign() {
    this.router.navigate(['/ideation/mycampaigns/create'], {relativeTo: this.route});
  }

  userprofile(event){
    this.hamburger=!this.hamburger;
     var effect = 'slide';
      // Set the options
      // for the effect type chosen
      var options = { direction:"left" };

      // Set the duration (default: 400 milliseconds)
      var duration = 500;

      event.stopPropagation();
      var vm=this;
      document.onclick = function(e){

            if(((<HTMLInputElement>e.target).id!='ham'&&(<HTMLInputElement>e.target).id!='menu')||(<HTMLInputElement>e.target).id=='')
            {

                vm.hamburger=false;
                vm.ref.detectChanges();
            }

          }


  }

  logout(){
    this.auth.logOut();
    this.router.navigate(['/signin']);

  }

}
