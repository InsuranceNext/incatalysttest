import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { IdeationComponent } from './components/ideation/ideation.component';
import { CampaignsComponent } from './components/campaigns/campaigns.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MyideasComponent } from './components/myideas/myideas.component';
import { CreatecampaignComponent } from './components/createcampaign/createcampaign.component';
import { RatingslistComponent } from './components/ratingslist/ratingslist.component';
import { RatingcampaignideaComponent } from './components/ratingcampaignidea/ratingcampaignidea.component';
import { IncatalysthomeComponent } from './components/incatalysthome/incatalysthome.component';
import { LoaderComponent } from './components/shared/loader/loader.component';
import { MycampaignsComponent } from './components/mycampaigns/mycampaigns.component';
import { MycampaignsviewideasComponent } from './components/mycampaignsviewideas/mycampaignsviewideas.component';
import { MycampaignsshareComponent } from './components/mycampaignsshare/mycampaignsshare.component';
import { SignupComponent } from './services/auth/signup/signup.component';
import { SigninComponent } from './services/auth/signin/signin.component';
import { ForgotpasswordComponent } from './services/auth/forgotpassword/forgotpassword.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import { UrlpopupComponent } from './components/urlpopup/urlpopup.component';
import { CampaignsubmitideaComponent } from './components/campaignsubmitidea/campaignsubmitidea.component';
import { IdeapopupComponent } from './components/ideapopup/ideapopup.component';
import { PlayvideoComponent } from './components/playvideo/playvideo.component';

import { AuthService } from './services/auth/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MustMatchDirective } from './services/helpers/must-match.directive';


import { FormsModule } from '@angular/forms';
import {PopoverModule} from '../../node_modules/ngx-popover';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LoaderService } from './services/loader.service';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './routing/app-routing.module';
import { BrowserModule } from '@angular/platform-browser';


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, HttpClientTestingModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserModule,
        FormsModule,
        PopoverModule,
        BrowserAnimationsModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule
      ],
      declarations: [
        AppComponent,
        AppComponent,
        IdeationComponent,
        CampaignsComponent,
        MustMatchDirective,
        DashboardComponent,
        MyideasComponent,
        CreatecampaignComponent,
        RatingslistComponent,
        RatingcampaignideaComponent,
        IncatalysthomeComponent,
        LoaderComponent,
        MycampaignsComponent,
        MycampaignsviewideasComponent,
        MycampaignsshareComponent,
        CampaignsubmitideaComponent,
        SignupComponent,
        SigninComponent,
        ForgotpasswordComponent,
        IdeapopupComponent,
        StarRatingComponent,
        UrlpopupComponent,
        PlayvideoComponent
      ],
      providers: [AuthService]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'INCatalyst7'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('INCatalyst7');
  });

  // it('should render title in a h1 tag', () => {
  //   // const fixture = TestBed.createComponent(AppComponent);
  //   // fixture.detectChanges();
  //   // const compiled = fixture.debugElement.nativeElement;
  //   // expect(compiled.querySelector('h1').textContent).toContain('Welcome to INCatalyst7!');

  //   const fixture = TestBed.createComponent(AppComponent);
  //   const htmlElement: HTMLElement = fixture.nativeElement;
  //   const divElement = htmlElement.querySelector('h1');
  //   fixture.detectChanges();
  //   console.log(divElement);
  //   expect(divElement.innerText).toEqual('Welcome to INCatalyst7!');


  // });
});
