import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IdeationComponent } from './../components/ideation/ideation.component';

import { IncatalysthomeComponent } from './../components/incatalysthome/incatalysthome.component';
import { CampaignsComponent } from './../components/campaigns/campaigns.component';

import { RatingslistComponent } from './../components/ratingslist/ratingslist.component';
import { RatingcampaignideaComponent } from './../components/ratingcampaignidea/ratingcampaignidea.component';
import { RatingsService } from '../services/ratings.service';
import { MyideasComponent } from './../components/myideas/myideas.component';

import { DashboardComponent } from './../components/dashboard/dashboard.component';
import { MycampaignsComponent } from './../components/mycampaigns/mycampaigns.component';

import { IncatalystResolverService } from './../services/incatalyst-resolver.service';
import { CampaignsService } from './../services/campaigns.service';
import { OthersService } from './../services/others.service';
import { MycampaignsviewideasComponent } from './../components/mycampaignsviewideas/mycampaignsviewideas.component';
import { MycampaignsshareComponent } from './../components/mycampaignsshare/mycampaignsshare.component';
import { CampaignsubmitideaComponent } from './../components/campaignsubmitidea/campaignsubmitidea.component';
import {CreatecampaignComponent} from './../components/createcampaign/createcampaign.component';
import { SignupComponent } from './../services/auth/signup/signup.component';
import { SigninComponent } from './../services/auth/signin/signin.component';
import { AuthGuard } from './../services/auth/auth-guard.service';
import { AuthGuardReverse } from './../services/auth/auth-guard-reverse.service';
import { ForgotpasswordComponent } from './../services/auth/forgotpassword/forgotpassword.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'signin',
    pathMatch: 'full'
  },
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [AuthGuardReverse]
  },
  {
    path: 'signin',
    component: SigninComponent,
    canActivate: [AuthGuardReverse]
  },
  { path: 'forgotpassword',
  component: ForgotpasswordComponent,
  canActivate: [AuthGuardReverse]
  },
  {
    path: 'incatalyst',
    component: IncatalysthomeComponent,
    canActivate: [AuthGuard]
  },

  {
  path: 'ideation',
  component: IdeationComponent,
   children:[
        {
          path: 'campaigns',
          component: CampaignsComponent,
          //canActivate: [AuthGuard]
        },
        {
            path:'campaigns/:id/ideas/submit/:ideaid',
            component: CampaignsubmitideaComponent,
            resolve: { campaign:CampaignsService},
            //canActivate: [AuthGuard]

        },
        {
            path:'campaigns/:id/ideas/submit',
            component: CampaignsubmitideaComponent,
            resolve: { campaign:CampaignsService},
            //canActivate: [AuthGuard]

        },
        {
          path: 'dashboard',
          component: DashboardComponent,
          //canActivate: [AuthGuard]
        },
        {
          path: 'mycampaigns',
          component: MycampaignsComponent,
          //canActivate: [AuthGuard]
        },
	      {
          path: 'mycampaigns/create',
          component: CreatecampaignComponent,
          //canActivate: [AuthGuard]
        },
        {
          path: 'mycampaigns/create/:id',
          component: CreatecampaignComponent,
          //canActivate: [AuthGuard]
        },
        {
      	  path:'mycampaigns/:id/ideas/list',
      	  component: MycampaignsviewideasComponent,
      	  resolve: { ideas:  IncatalystResolverService,campaign:CampaignsService },
          //canActivate: [AuthGuard]
        },
	       {
            path:'mycampaigns/:id/ideas/list',
            component: MycampaignsviewideasComponent,
            resolve: { ideas:  IncatalystResolverService,campaign:CampaignsService },
            //canActivate: [AuthGuard]
        },
        {
          path: 'rating',
          component: RatingslistComponent,
          //canActivate: [AuthGuard]
        },
        {
          path: 'createcampaign',
          component: CreatecampaignComponent,
          //canActivate: [AuthGuard]
        },

        {
          path: 'rating/:id/rate',
          component: RatingcampaignideaComponent,
           resolve: { campaign:CampaignsService},
           //canActivate: [AuthGuard]
        },
        {
            path:'mycampaigns/:id/share',
            component: MycampaignsshareComponent,
            resolve: { campaign:CampaignsService},
            //canActivate: [AuthGuard]
        },
        {
          path: 'myideas',
          component: MyideasComponent,
          //canActivate: [AuthGuard]
        },
   ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
