import { TestBed } from '@angular/core/testing';

import { IncatalystResolverService } from './incatalyst-resolver.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './auth/auth.service';

describe('IncatalystResolverService', () => {

  let serviceInstance: IncatalystResolverService;
  const defaultToken = 'abcd1234';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [AuthService ]
    });
    const cognitoUser = {
      getSession: (callback) => {
        return null;
      }
    };

    // AuthService is not fully mocked here. Unable to set the token property in incatalyst-resolver.service.ts
    const authService = TestBed.get(AuthService);
    const authUserSpy = spyOn(authService, 'getAuthenticatedUser').and.returnValue(cognitoUser);
    serviceInstance = TestBed.get(IncatalystResolverService);
  });

  it('should be created', () => {
    const service: IncatalystResolverService = TestBed.get(IncatalystResolverService);
    expect(service).toBeTruthy();
  });
});
