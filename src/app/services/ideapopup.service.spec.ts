import { TestBed } from '@angular/core/testing';

import { IdeapopupService } from './ideapopup.service';

describe('IdeapopupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IdeapopupService = TestBed.get(IdeapopupService);
    expect(service).toBeTruthy();
  });
});
