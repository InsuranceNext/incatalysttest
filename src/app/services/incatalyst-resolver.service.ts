import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from './auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class IncatalystResolverService implements Resolve<any> {
  token: any;
  constructor(private http: HttpClient, private auth: AuthService) {
    this.auth.getAuthenticatedUser().getSession((err, session) => {
      this.token = session.getIdToken().getJwtToken();
    });
  }

  resolve(route: ActivatedRouteSnapshot) {

    // return this.http.get('http://localhost:3000/api/ideas', {
    //     params: {
    //       campaignid: route.params.id
    //
    //     }
    //   })

    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', this.token);

    const params = new HttpParams().set('campaignid', route.params.id);

    return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ideas', { headers, params });
  }
}
