import { TestBed } from '@angular/core/testing';

import { OthersService } from './others.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './auth/auth.service';

describe('OthersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [AuthService]
    });

  }

  );

  it('should be created', () => {
    const service: OthersService = TestBed.get(OthersService);
    expect(service).toBeTruthy();
  });
});
