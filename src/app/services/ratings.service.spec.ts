import { TestBed } from '@angular/core/testing';

import { RatingsService } from './ratings.service';
import { HttpClientModule } from '@angular/common/http';

describe('RatingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
  });

  it('should be created', () => {
    const service: RatingsService = TestBed.get(RatingsService);
    expect(service).toBeTruthy();
  });
});
