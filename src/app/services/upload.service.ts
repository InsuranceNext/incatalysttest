import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from './auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient,private auth:AuthService) { }

  uploadideadoc(file,filename,token){
    const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token,

      });
      const params = new HttpParams().set('user', this.auth.getAuthenticatedUser().getUsername());
      const httpOptions = {
        headers: headers,
        params: params
      };
      const body = {
        key1: 'value1',
        key2: 'value2',
        bucketName: 'palash',
        fileName: 'ideas/' + filename,
        fileContent: file
      };
      return this.http.post('https://krz40kq5db.execute-api.us-east-1.amazonaws.com/Dev/files', body, httpOptions);
  }
  downloadideadoc(file,token){

    const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token,
        // 'Access-Control-Allow-Origin': '*',
        // 'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        // 'Access-Control-Allow-Headers': 'X-Requested-With,content-type'
      });
      const params = new HttpParams().set('user', this.auth.getAuthenticatedUser().getUsername());
      const httpOptions = {
        headers: headers,
        params: params
      };
      const body = {
        bucketName: 'palash',
        fileName: file

      };
      return this.http.post('https://krz40kq5db.execute-api.us-east-1.amazonaws.com/Dev/download', body, httpOptions);
  }
  uploadcampaigndoc(file,filename,type,token){
    const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token,
        // 'Access-Control-Allow-Origin': '*',
        // 'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        // 'Access-Control-Allow-Headers': 'X-Requested-With,content-type'
      });
      const params = new HttpParams().set('user', this.auth.getAuthenticatedUser().getUsername());
      const httpOptions = {
        headers: headers,
        params: params
      };
      if(type=="image")
      {
      const body = {
        key1: 'value1',
        key2: 'value2',
        bucketName: 'palash',
        fileName: 'campaignsimages/' + filename,
        fileContent: file
      };
      return this.http.post('https://krz40kq5db.execute-api.us-east-1.amazonaws.com/Dev/files', body, httpOptions);
    }
    else{
      const body = {

        bucketName: 'palash',
        fileName: 'campaigns/' + filename,
        fileContent: file
      };
      return this.http.post('https://krz40kq5db.execute-api.us-east-1.amazonaws.com/Dev/files', body, httpOptions);
    }

  }
  downloadcampaigndoc(file,token){
    const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token,
        // 'Access-Control-Allow-Origin': '*',
        // 'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        // 'Access-Control-Allow-Headers': 'X-Requested-With,content-type'
      });
      const params = new HttpParams().set('user', this.auth.getAuthenticatedUser().getUsername());
      const httpOptions = {
        headers: headers,
        params: params
      };
      const body = {
        bucketName: 'palash',
        fileName: file,

      };
      return this.http.post('https://krz40kq5db.execute-api.us-east-1.amazonaws.com/Dev/download', body, httpOptions);
  }
}
