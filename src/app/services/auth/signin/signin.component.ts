import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  emailVerificationMessage: boolean = false;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSignin(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;

    this.auth.signIn(email, password).subscribe(
      (data) => {

      this.router.navigateByUrl('/incatalyst');
    },
      (err) => {
        this.emailVerificationMessage = true;

      }
    );
  }
}
