import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  verificationCodeSent: boolean = false;
  userNotFound: boolean = false;
  codeMatch: boolean = true;
  model: any = {};

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onForgotPassword(form: NgForm) {
    const email = form.value.email;
    this.auth.forgotPassword(email).subscribe(
      (data) => {
        this.verificationCodeSent = true;
        this.userNotFound = false;
      },
        (err) => {

          if (err.code === 'UserNotFoundException') {
            this.userNotFound = true;
          }
        }
    );
  }

  onSubmitNewPassword(form: NgForm) {

    const code = form.value.code;
    const newPassword = form.value.newPassword;
    const confirmNewPassword = form.value.confirmNewPassword;

    this.auth.createNewPassword(code, newPassword).subscribe(
      () => {

        this.router.navigateByUrl('/');
      },
      (err) => {

        if (err.code === 'CodeMismatchException') {
          this.codeMatch = false;
        }
      }
    );

  }

}
