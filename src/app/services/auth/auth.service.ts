import { CognitoUserPool, CognitoUserAttribute, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Headers } from "@angular/http";

// https://docs.aws.amazon.com/cognito/latest/developerguide/tutorial-integrating-user-pools-javascript.html
const poolData = {
    UserPoolId: 'us-east-1_vBdswJa6Y', // The user pool id needs to be put here.
    ClientId: '9nov94hg9hc07srsmp7m2rr2e', // The client id needs to be put here.
    //     UserPoolId: 'us-east-1_6aUIUskE8', // Your user pool id here
    // ClientId: '7k7smtobde6uv4kcg1mupk58cr' // Your client id here
};

const userPool = new CognitoUserPool(poolData);

@Injectable()
export class AuthService {
    cognitoUser: any;
    constructor(private http: HttpClient) { }

    getToken() {
        const authenticatedUser = this.getAuthenticatedUser();
        if (authenticatedUser == null) {

            return;
        }
        return Observable.create(observer => {
            authenticatedUser.getSession((err, session) => {
                if (err) {
                    console.log(err);
                    return;
                } else {

                    const token = session.getIdToken().getJwtToken();

                    const headers = new Headers();
                    headers.append('Authorization', token);
                    headers.append('Content-Type', 'application/json');
                    headers.append('Access-Control-Allow-Origin', '*');
                    headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
                    headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');


                    observer.next(headers);
                }
            });
            // return headers;
        })
    }

    register(email, password, firstName, lastName) {
        const attributeList = [];

        var dataEmail = {
            Name: 'email',
            Value: email
        };
        var dataFirstName = {
            Name: 'name',
            Value: firstName
        };
        var dataLastName = {
            Name: 'family_name',
            Value: lastName
        };

        var attributeEmail = new CognitoUserAttribute(dataEmail);
        var attributeFirstName = new CognitoUserAttribute(dataFirstName);
        var attributeLastName = new CognitoUserAttribute(dataLastName);

        attributeList.push(attributeEmail);
        attributeList.push(attributeFirstName);
        attributeList.push(attributeLastName);

        return Observable.create(observer => {
            userPool.signUp(email, password, attributeList, null, (err, result) => {
                if (err) {

                    observer.error(err);
                } else {
                    this.cognitoUser = result.user;

                    observer.next(result);
                    observer.complete();
                }
            });
        });
    }
    createuserentry(email, firstname, lastname, usersub) {
        //https://pmh1rq2oei.execute-api.us-east-1.amazonaws.com/dev/api/auth/signup
        //http://localhost:3000/api/auth/signup
        return this.http.post('https://pmh1rq2oei.execute-api.us-east-1.amazonaws.com/dev/api/auth/signup',
            {
                "email": email,
                "firstName": firstname,
                "lastName": lastname,
                "_id": usersub
            });
    }
    getcurrentuserfromdb() {

        const params = new HttpParams().set('id', this.getAuthenticatedUser().getUsername());
        //var url='https://pmh1rq2oei.execute-api.us-east-1.amazonaws.com/dev/api/auth/signin?id='+this.getAuthenticatedUser().getUsername();
        return this.http.post('https://pmh1rq2oei.execute-api.us-east-1.amazonaws.com/dev/api/auth/signin', {}, { params });
    }


    confirmAuthCode(code) {
        const user = {
            Username: this.cognitoUser.username,
            Pool: userPool
        };
        return Observable.create(observer => {
            const cognitoUser = new CognitoUser(user);
            cognitoUser.confirmRegistration(code, true, (err, result) => {
                if (err) {

                    observer.error(err);
                }

                observer.next(result);
                observer.complete();
            });
        });
    }

    signIn(email, password) {
        const authenticationData = {
            Username: email,
            Password: password
        };
        const authenticationDetails = new AuthenticationDetails(authenticationData);

        const userData = {
            Username: email,
            Pool: userPool
        };
        const cognitoUser = new CognitoUser(userData);

        return Observable.create(observer => {
            cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: (result) => {

                    observer.next(result);
                    observer.complete();
                },
                onFailure: (err) => {
                    // console.log(err);
                    observer.error(err);
                }
            });
        });
    }

    isLoggedIn() {
        return userPool.getCurrentUser() != null;
    }

    getAuthenticatedUser() {
        // to get the current user from the local storage
        return userPool.getCurrentUser();
    }

    logOut() {
        this.getAuthenticatedUser().signOut();
        this.cognitoUser = null;
    }

    forgotPassword(username) {

        const cognitoUser = new CognitoUser({
            Username: username,
            Pool: userPool
        });
        this.cognitoUser = cognitoUser;


        return Observable.create(observer => {
            cognitoUser.forgotPassword({
                onSuccess: (result) => {

                    observer.next(result);
                    observer.complete();
                },
                onFailure: (err) => {

                    observer.error(err);
                }
            });
        });

    }

    createNewPassword(code, newPassword) {
        const user = {
            Username: this.cognitoUser.username,
            Pool: userPool
        };

        return Observable.create(observer => {
            const cognitoUser = new CognitoUser(user);
            cognitoUser.confirmPassword(code, newPassword, {
                onSuccess: () => {
                    observer.next();
                    observer.complete();
                },
                onFailure: (err) => {
                    observer.error(err);
                }
            });
        });
    }


}
