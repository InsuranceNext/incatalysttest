import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  confirmCode: boolean = false;
  codeWasConfirmed: boolean = false;
  usernameExists: boolean = false;
  error: string = '';
  model: any = {};

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onRegister(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    const firstName = form.value.firstName;
    const lastName = form.value.lastName;

    this.auth.register(email, password, firstName, lastName).subscribe(
      (data) => {
        this.confirmCode = true;



        this.auth.createuserentry(email,firstName, lastName,data.userSub).subscribe(
          (data)=>{
          
            this.router.navigateByUrl('/signin');
          }
        )
      },
      (err) => {
        this.error = 'Registration Error has occurred';
        if (err.code === 'UsernameExistsException') {
          this.usernameExists = true;
        }
      }
    );
  }

  onCodeSubmit(form: NgForm) {
    const code = form.value.code;

    this.auth.confirmAuthCode(code).subscribe(
      (data) => {
        this.codeWasConfirmed = true;
        this.confirmCode = false;
      },
      (err) => {
        this.error = 'Confirm Authorization Error has occurred';
      }
    );
  }

}
