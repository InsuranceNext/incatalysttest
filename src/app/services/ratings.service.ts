import { Injectable } from '@angular/core';
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient,HttpHeaders,HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RatingsService implements Resolve<any>{

  constructor(private http: HttpClient) { }
 resolve(route: ActivatedRouteSnapshot){
     return this.http.get('http://localhost:3000/campaigns' ,{
         params: {
           campaignid: route.params.id

         }
       });
 }

getratingsforideas(ideasid,token){

  // const params = new HttpParams().append('idea_ids[]', JSON.stringify(ideasid));
  //  return this.http.get('http://localhost:3000/api/ratings' ,{params});
   let headers = new HttpHeaders();
   headers = headers.append('Content-Type', 'application/json');
   headers = headers.append('Authorization', token);
   const params = new HttpParams().set('idea_ids[]', JSON.stringify(ideasid));


   return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ratings', { headers, params });
}

getratingsforcampaigns(campaignids,token){
  let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', token);
    const params = new HttpParams().set('campaign_ids[]', JSON.stringify(campaignids));


    return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ratings', { headers, params });
  // const params = new HttpParams().append('campaign_ids[]', JSON.stringify(campaignids));
  //   return this.http.get('http://localhost:3000/api/ratings' ,{params});
}

getuserrating(ideaid,user,token){

  // let params = new HttpParams()
  // .set('idea_id', ideaid)
  // .set('user', user);
  //
  //  return this.http.get('http://localhost:3000/api/ratings' ,{params});
   let headers = new HttpHeaders();
   headers = headers.append('Content-Type', 'application/json');
   headers = headers.append('Authorization', token);
   const params = new HttpParams()
   .set('idea_id', ideaid)
   .set('user', user);

   return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ratings', { headers, params });
}
getidearating(ideaid,token){

  // let params = new HttpParams().set('idea_id', ideaid);


  //  return this.http.get('http://localhost:3000/api/ratings' ,{params});
  let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);
  const params = new HttpParams().set('idea_id', ideaid);


  return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ratings', { headers, params });
}
saverating(body,token){

//     let headers = new HttpHeaders();
//   headers = headers.set('Content-Type', 'application/json; charset=utf-8');
// const headers = {'Content-Type': 'application/json','Access-Control-Allow-Origin': '*'};
//   return this.http.post('http://localhost:3000/api/ratings',body, { headers});
let headers = new HttpHeaders();
headers = headers.append('Content-Type', 'application/json');
headers = headers.append('Authorization', token);

return this.http.post('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ratings',JSON.stringify(body), { headers});
}
getideadetails(idea:any,token){
// return this.http.get('http://localhost:3000/campaigns' ,{
//          params: {
//            campaignid:idea

//          }
//        });
let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);
  const params = new HttpParams();
  params.set('campaignid', idea);

  return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/campaigns', { headers, params });
}
// getidearateddetails(userid:any,ideaid:any){
// return this.http.get('http://localhost:3000/campaigns' ,{
//          params: {
//            campaignid:userid
//
//          }
//        });
//
// }
}
