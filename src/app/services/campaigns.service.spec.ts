import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpHeaders, HttpParams,HttpClientModule } from '@angular/common/http';
import { MockBackend,MockConnection } from '@angular/http/testing';
import { addProviders, inject } from '@angular/core/testing';

import { CampaignsService } from './campaigns.service';
import { AuthService } from './auth/auth.service';

describe('CampaignsService', () => {
   beforeEach(() =>{
    //  TestBed.configureTestingModule({
    //  imports: [HttpClientModule],
    //  providers: [CampaignsService,AuthService]
   //});
  });
  let service:CampaignsService;
  let auth:AuthService;
  let http:HttpClientModule;
  it('should be created', () => {
     //const service: CampaignsService = TestBed.get(CampaignsService);
     //expect(service).toBeTruthy();
     let sp=spyOn( CampaignsService.prototype, 'getToken');
     service = new CampaignsService(http,auth);
     expect(service).toBeTruthy();
   });
  });
 describe('Given CampaignsService is declared,when it is initialized',() => {
  let service:CampaignsService;
  let auth:AuthService;
  let http:HttpClientModule;
  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [HttpClientModule],
    providers: [CampaignsService,AuthService]
  });
  auth = TestBed.get(AuthService);
  });
  it('should call get Token Function that returns session', () => {
    let sp=spyOn( auth, 'getAuthenticatedUser').and.returnValue({
      'Session': null,
      'endpoint': "https://cognito-idp.us-east-1.amazonaws.com/",
      'userAgent': "aws-amplify/0.1.x js",
      'keyPrefix': "CognitoIdentityServiceProvider.9nov94hg9hc07srsmp7m2rr2e",
      'endpoint': "https://cognito-idp.us-east-1.amazonaws.com/",
      'userAgent': "aws-amplify/0.1.x js",
      'clientId': "9nov94hg9hc07srsmp7m2rr2e",
      'length': 5,
      'userPoolId': "us-east-1_vBdswJa6Y",
      'signInUserSession': null,
      'userDataKey': "CognitoIdentityServiceProvider.9nov94hg9hc07srsmp7m2rr2e.916dfc9e-1913-4bc2-af7f-71beb085a2f8.userData",
      'username': "916dfc9e-1913-4bc2-af7f-71beb085a2f8"
    });
    let sp1=spyOn(sp,'getSession');
    let sp2=spyOn(sp1,'getJwtToken');
    expect(sp2).toBeTruthy();
  });
  it('should call get Token function that throws an error', () => {
    spyOn(authservice,'getAuthenticatedUser').and.throwError(function(){
      return null;
    });
    service = new CampaignsService(http,auth);
    expect(service).toBeTruthy();
});
});

describe('Given CampaignsService is declared and initialized, when calling getcampaigns function',()=>{
  let campaignsservice: CampaignsService;
  let authservice: AuthService;
  let http:HttpClient;
  beforeEach(inject([CampaignsService, MockBackend], (campaignsservice: CampaignsService, mockBackend: MockBackend) => {
    service = campaignsservice;
    backend = mockBackend;
  }));
  let campaigns:any=[{"name":"campaign1","campaignimage":"/campaign/image.jpg"},{"name":"campaign2","campaignimage":"/campaign/image.jpg"}]
  it('should be truthy',()=>{
      service.getcampaigns(service.token).toBeTruthy();
  })
  it('should call the backend and return the response',()=>{
    backend.connections.subscribe((connection: MockConnection) => {
      let options = new ResponseOptions({
        body: JSON.stringify(campaigns)
      });
      connection.mockRespond(new Response(options));
    });
    service
      .getcampaigns(service.token)
      .subscribe((response) => {
        expect(response.json()).toEqual(campaigns);
        done();
    });
  });
  it('should call the backend and return an error',()=>{
    backend.connections.subscribe((connection: MockConnection) => {
      connection.mockRespond(new Error("Bad Request"));
    });
    service
      .getcampaigns(service.token)
      .subscribe((err) => {
        expect(err).toEqual(new Error("Bad Request"));
        done();
      });
    });
  });

describe('Given CampaignsService is declared and initialized, when calling getcampaignbyid function',()=>{
  let campaignsservice: CampaignsService;
  let authservice: AuthService;
  let http:HttpClient;
  beforeEach(inject([CampaignsService, MockBackend], (campaignsservice: CampaignsService, mockBackend: MockBackend) => {
    service = campaignsservice;
    backend = mockBackend;
  }));
  let campaign:any={"name":"campaign1","campaignimage":"/campaign/image.jpg"};
  let id:any;
  it('should be truthy',()=>{
    service.getCampaignById(service.token).toBeTruthy();
  });
  it('should call the backend and return the response',()=>{
    backend.connections.subscribe((connection: MockConnection) => {
      let options = new ResponseOptions({
        body: JSON.stringify(campaign)
      });
      connection.mockRespond(new Response(options));
    });
    service
      .getCampaignById(id,service.token)
      .subscribe((response) => {
        expect(response.json()).toEqual(campaign);
        done();
      });
    });
  });

describe("Given CampaignsService is declared and it is initialized,when getting campaigns for the user",()=>{
  //let campaignsservice: CampaignsService;
  let authservice: AuthService;
  let http:HttpClient;
  let service:CampaignsService;
  //beforeEach(inject([CampaignsService, MockBackend], (campaignsservice: CampaignsService, mockBackend: MockBackend) => {
  //  service = new CampaignsService(http,authservice);
  //  backend = mockBackend;
  //}));

  beforeEach(()=>{
    authservice=new AuthService(http);
    service=new CampaignsService(http,authservice);
  })
  let email:any;
  let token:any;
  it("should be truthy",()=>{
    service=new CampaignsService(http,authservice);
    expect(service.getcampaignsbyuser(email,token)).toBeTruthy();
  });
  it("should call the backend with specific URL",()=>{
    backend.connections.subscribe((connection: MockConnection) => {
     expect(connection.request.url).toEqual('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/campaigns');
     });
  });
});
