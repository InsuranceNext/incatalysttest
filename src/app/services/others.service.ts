import { Injectable } from '@angular/core';
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from './auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class OthersService implements Resolve<any> {

  constructor(private http: HttpClient,private auth:AuthService) { }

  resolve(route: ActivatedRouteSnapshot){
    return this.http.get('http://localhost:3000/api/others' ,{
         params: {
           campaignid: route.params.id
         }
       })
  }
save(body,token){
  let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);
  const url = 'https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/others';
  return this.http.post(url, JSON.stringify(body), { headers });
    // let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    // const headers = {'Content-Type': 'application/json','Access-Control-Allow-Origin': '*'};
    // return this.http.post('http://localhost:3000/api/others',body, { headers});
  }
getquestion(id,token){

  // return this.http.get('http://localhost:3000/api/others',{
  //       params: {
  //         campaignid: id
  //       }
  //     })

  let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);

  const params = new HttpParams().set('campaignid', id);

  return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/others', { headers, params });
  }
  getquestionbyid(id,token){

    // return this.http.get('http://localhost:3000/api/others',{
    //       params: {
    //         campaignid: id
    //       }
    //     })

    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', token);

    const params = new HttpParams().set('_id', id);

    return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/others', { headers, params });
    }
}
