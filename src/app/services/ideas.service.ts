import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class IdeasService {

  constructor(private http: HttpClient) { }
  getideasforcampaign(campaignid,token) {
    let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);
  const params = new HttpParams().set('campaignid', campaignid);

  return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ideas', { headers, params });
    // return this.http.get('http://localhost:3000/api/ideas', {
    //   params: {
    //     campaignid: campaignid
	  //    }
	  //   })
		  }


 getIdeasById(ideaid,token){
  // return this.http.get('http://localhost:3000/ideas/', {
  //  params: {
  //     campaignid: ideaid

  //  }
  // })
  let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);
  const params = new HttpParams().set('_id', ideaid)


  return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ideas', { headers, params });
}

 getIdeasforCampaignidArray(campaignids,token) {

    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', token);
    const params = new HttpParams().set('campaign_ids[]', JSON.stringify(campaignids));
    // let params = new HttpParams();
    // params = params.append('campaign_ids', campaignids.join(', '));
    //return this.http.get('http://localhost:3000/api/ideas/', {params});
        // params: {
        //   campaign_ids: campaignids
        //
        // }
      //})

    return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ideas', { headers, params });
}

 getideasforuser(userId,token){
 // return this.http.get('http://localhost:3000/api/ideas/', {
 //      params: {
 //        user: userId
 //
 //      }
 //    })
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', token);

    const params = new HttpParams().set('user', userId);


    return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ideas', { headers, params });
 }
 getideabyid(ideaid,token){
 // return this.http.get('http://localhost:3000/api/ideas/', {
 //      params: {
 //        id: ideaid
 //
 //      }
 //    })

    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', token);

    const params = new HttpParams().set('ideaid', ideaid);


    return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ideas', { headers, params });
 }

 downloadCampaignIdea(campaignid,template,name,token){
  let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);
  const params = new HttpParams()
  .set('campaignID', campaignid)
  .set('template',template)
  .set('campaignName',name);

  return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ideas/saveexcel', { headers, params });
 }

 downloadAllIdea(token){
  let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);
  return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ideas/saveallideas', { headers});
 }

 downloadRatedMembers(userid,token){
  let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);
  const params = new HttpParams().set('user', userid);
  return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/rateduser', { headers,params});
 }


 saveIdeaForCampaign(idea:any,token){

   // const url = 'http://localhost:3000/api/ideas';
   // // let headers = new HttpHeaders();
   // // headers = headers.set('Content-Type', 'application/json; charset=utf-8');
   // const headers = {'Content-Type': 'application/json','Access-Control-Allow-Origin': '*'};
   // return this.http.post(url, idea, {headers});


   let headers = new HttpHeaders();
   headers = headers.append('Content-Type', 'application/json');
   headers = headers.append('Authorization', token);


   const url = 'https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/ideas';
   return this.http.post(url, idea, { headers });

 }
}
