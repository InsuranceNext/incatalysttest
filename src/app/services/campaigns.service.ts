import { Injectable } from '@angular/core';
import { Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from './auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignsService implements Resolve<any> {
token:any;
  constructor(private http: HttpClient,private auth:AuthService) {
    this.auth.getAuthenticatedUser().getSession((err, session) => {
      this.token = session.getIdToken().getJwtToken();
         });
  }

  getcampaigns(token) {

        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Authorization', token);

        const params = new HttpParams().set('user', this.auth.getAuthenticatedUser().getUsername());

        return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/campaigns', { headers, params });
 }
 getCampaignById(id:any,token){

    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', token);

    const params = new HttpParams().set('campaignid', id);

    return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/campaigns', { headers, params });
 }
 getcampaignsbyuser(email:any,token){

    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', token);

    const params = new HttpParams().set('user_email_share', email);

    return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/campaigns', { headers, params });

    // let headers = new HttpHeaders();
    // headers = headers.append('Content-Type', 'application/json');
    // headers = headers.append('Authorization', token);
    //
    // const params = new HttpParams().set('user_email_share', email);
    //
    // return this.http.get('http://localhost:3000/api/campaigns', { headers, params });

 }
 getcampaignsbyuserrating(email:any,token){

    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', token);

    const params = new HttpParams().set('user_email_rating', email);

    return this.http.get(' https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/campaigns', { headers, params });
 }
 resolve(route: ActivatedRouteSnapshot){

       let headers = new HttpHeaders();
       headers = headers.append('Content-Type', 'application/json');
       headers = headers.append('Authorization', this.token);

       const params = new HttpParams().set('campaignid', route.params.id);

       return this.http.get('https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/campaigns', { headers, params });
 }
createCampaign(body,token){

  let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);


  const url = 'https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/campaigns';
  return this.http.post(url, JSON.stringify(body), { headers });

}
updateforshare(body,mailusers,token){

  let headers = new HttpHeaders();
  headers = headers.append('Content-Type', 'application/json');
  headers = headers.append('Authorization', token);



  const url = 'https://thr0m23bzf.execute-api.us-east-1.amazonaws.com/dev/api/campaigns/share';
  return this.http.post(url, JSON.stringify({"campaign":body,"mailusers":mailusers}), { headers });


}
}
