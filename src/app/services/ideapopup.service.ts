import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IdeapopupService {
     currentbmcpage:any = "xxx";
     customervalue:any="uncheck";
     foundervalue:any="uncheck";
     valuepropositionvalue:any = "uncheck";
     keyactivitiesvalue:any = "uncheck";
     keyresourcesvalue:any = "uncheck";
     coststructurevalue:any = "uncheck";
     revenuemodelvalue:any = "uncheck";
     channelvalue:any = "uncheck";
     partnersvalue:any = "uncheck";
     questionarray:any;
     idea:any;
  constructor() { }

    checkservice(bmcpagename:String,idea:any,questionarray:any)
    {

      this.currentbmcpage = bmcpagename;
      this.idea=idea;
      this.questionarray=questionarray;
    
    }

      customer(answer)
    {
      if(answer == "" || answer == undefined)
      {
      this.customervalue = "unchecked";
      }
      else {
        this.customervalue="checked";
      }
    }
     founders(answer1)
    {
      if(answer1 == "" || answer1 == undefined)
      {
      this.foundervalue = "unchecked";
      }
      else {
        this.foundervalue="checked";
      }

    }
     keyactivities(answer)
    {
      if(answer == "" || answer == undefined)
      {
      this.keyactivitiesvalue = "unchecked";
      }
      else {
        this.keyactivitiesvalue="checked";
      }

    }
     keyresources(answer)
    {
      if(answer == "" || answer == undefined)
      {
      this.keyresourcesvalue = "unchecked";
      }
      else {
        this.keyresourcesvalue="checked";
      }

    }
     channel(answer)
    {
      if(answer == "" || answer == undefined)
      {
      this.channelvalue = "unchecked";
      }
      else {
        this.channelvalue="checked";
      }

    }
     partners(answer )
    {
      if(answer == "" || answer == undefined)
      {
      this.partnersvalue = "unchecked";
      }
      else {
        this.partnersvalue="checked";
      }

    }
     valueproposition(answer)
    {
      if(answer == "" || answer == undefined)
      {
      this.valuepropositionvalue = "unchecked";
      }
      else {
        this.valuepropositionvalue="checked";
      }

    }
     coststructure(answer)
    {
      if(answer == "" || answer == undefined)
      {
      this.coststructurevalue = "unchecked";
      }
      else {
        this.coststructurevalue="checked";
      }

    }
     revenuemodel(answer)
    {
      if(answer == "" || answer == undefined)
      {
      this.revenuemodelvalue = "unchecked";
      }
      else {
        this.revenuemodelvalue="checked";
      }

    }




}
